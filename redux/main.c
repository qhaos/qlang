#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "code.h"
#include "file.h"
#include "lex.h"
#include "lisp.h"
#include "parser.h"
#include "print.h"
#include "prune.h"
#include "types.h"
#include "type_pass.h"
#include "vec.h"


static int e = 0;

void file_callback(struct file* f) {
	e += lex(f);

	printf("%s\n", f->name);

	struct ast* ast = parse(f);
	lispify(ast);
	printf("After sorting:\n");
	type_pass(ast);
	sort_pass(ast);
	lispify(ast);
	type_pass(ast);
	codegen(ast);
	printf("Codegen (%zu):\n", program_len);
	print_code();
}

int main(int argc, char* argv[]) {
	vec(struct file*) inputs = vec_new(struct file*, argc == 1? 1 : argc-1);

	if(argc > 1) {
		for(int i = 1; i < argc; i++) {
			FILE* in;
			int is_stdin = strcmp(argv[i], "-") == 0;
			if(!is_stdin)
				in = fopen(argv[i], "r");
			else
				in = stdin;
			if(!in) {
				perror("open");
				continue;
			}
			vec_push(inputs, readfile(in, is_stdin? "<stdin>" : argv[i]));
			fclose(in);
		}
	} else {
		vec_push(inputs, readfile(stdin, "<stdin>"));
	}

	vec_foreach(inputs, file_callback);
}

