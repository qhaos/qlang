#include <stdio.h>

#include "iden.h"
#include "lisp.h"
#include "parser.h"
#include "scope.h"
#include "sym.h"

static void lispify_type(struct type* type) {
	switch(type->type_type) {
		case TYPE_ARRAY:
			printf("(array ");
			lispify_type(type->child);
			printf(")");
		break;
		case TYPE_PTR:
			printf("(ptr ");
			lispify_type(type->child);
			printf(")");
		break;
		case TYPE_ENUM:
		case TYPE_STRUCT:
		case TYPE_UNION:
			printf("%s", identbl[type->sym->iden].data);
		break;
		case TYPE_BUILTIN:
			printf("%s", keywords[type->keyword]);
		break;
	}
}

static void lispify_expr(struct expr* expr) {
	if(!expr) {
		puts("0HF*OK");
		return;
	}
	switch(expr->expr_type) {
		case EXPR_CAST:
			printf("(cast ");
			lispify_type(expr->type);
			putchar(' ');
			lispify_expr(expr->child);
		break;
		case EXPR_BINOP:
			printf("(%s ", ops[expr->binop.op]);
			lispify_expr(expr->binop.left);
			putchar(' ');
			lispify_expr(expr->binop.right);
			printf(")");
		break;
		case EXPR_UNOP:
			printf("(%s ", ops[expr->unop.op]);
			lispify_expr(expr->unop.child);
			printf(")");
		break;
		case EXPR_NUM:
			printf("%ld", expr->num);
		break;
		case EXPR_VAR:
			printf("%s", identbl[expr->sym->iden].data);
		break;
		case EXPR_FCALL:
			printf("(fcall %s", identbl[expr->fcall.sym->iden].data);
			for(size_t i = 0; i < vec_len(expr->fcall.args); i++) {
				putchar(' ');
				lispify_expr(expr->fcall.args[i]);
			}
			printf(")");
		break;
		case EXPR_IDX:
			printf("(idx ");
			lispify_expr(expr->idx.child);
			putchar(' ');
			lispify_expr(expr->idx.idx);
			printf(")");
		break;
	}
}

static void lispify_stmt(struct ast* ast) {
	if(!ast)
		return;


	switch(ast->stmt_type) {
		case STMT_IF:
			printf("(if ");
			lispify_expr(ast->if_stmt.condition);
//			putchar(' ');
			putchar('\n');
			lispify_stmt(ast->if_stmt.stmt);

			for(struct ast* chain = ast->if_stmt.else_chain; chain; chain = chain->if_stmt.else_chain) {
				if(chain->if_stmt.condition) {
					printf(" else-if ");
					lispify_expr(ast->if_stmt.condition);			
					putchar(' ');
				} else {
					printf(" else ");
				}
				lispify_stmt(chain->if_stmt.stmt);
			}

			printf(")");
		break;
		case STMT_EXPR:
			lispify_expr(ast->expr);
			putchar('\n');
		break;
		case STMT_VARDEC:
			printf("(vardef ");
			lispify_type(ast->vardec.sym->type);
			printf(" %s", identbl[ast->vardec.sym->iden].data);
			if(ast->vardec.initializer) {
				printf(" ");
				lispify_expr(ast->vardec.initializer);
			}
			printf(")\n");
		break;
		case STMT_BLOCK:
			printf("{\n");
			lispify_stmt(ast->block_stmt.code);
			printf("\n}");
		break;
	}
	if(ast->next)
		putchar(' ');
	lispify_stmt(ast->next);
}

void lispify(struct ast* ast) {
	if(!ast)
		return;


	switch(ast->ast_type) {
		case AST_STMT:
			lispify_stmt(ast);
		break;
		case AST_FUNDECL:
			struct symbol* sym = ast->fundecl.sym;
			if(ast->fundecl.is_complete) {		
				printf("(deffun %s (", identbl[sym->iden].data);	
			} else {
				printf("(declfun %s ", identbl[sym->iden].data);
			}
			lispify_type(sym->fun.ret_type);
			for(size_t i = 0; i < vec_len(sym->fun.args); i++) {
				putchar(' ');
				lispify_type(sym->fun.args[i]->type);
				if(ast->fundecl.is_complete) {
					printf(" %s", identbl[sym->fun.args[i]->iden].data);
				}
			}
			if(!ast->fundecl.is_complete) {
				printf(")");
				break;
			}

			printf(") ");
			lispify_stmt(sym->fun.code);
			printf(")");
		break;
		default:
		break;
	}

	putchar('\n');
	lispify(ast->next);
}
