#ifndef FILE_H_
#define FILE_H_

#include <stdio.h>

struct token;

struct file {
	int row;
	int col;
	int rows;
	char** lines;
	const char* name;
	struct token* tokens;
	struct token** tail;
};

struct file* readfile(FILE* in, const char* filename);

#endif
