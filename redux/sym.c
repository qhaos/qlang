#include "lex.h"
#include "scope.h"
#include "sym.h"

struct symbol* variable_declaration(struct scope* scope, struct type* type, struct token* tok) {
	struct symbol* sym = calloc(sizeof(struct symbol), 1);
	sym->symbol_type = SYM_VARIABLE;
	sym->defined = tok;
	sym->iden = tok->iden;
	sym->type = type;
	if(scope)
		return scope_add(scope, sym, NULL);
	else
		return sym;
}
