#ifndef AST_H_
#define AST_H_

#include "lex.h"
#include "types.h"
#include "vec.h"

/* Attempted grammer specification (ambigious due to operator precedence)

	stmt :- expr ';' | if-stmt | '{' stmt* '}' | var-decl
	if-stmt-base :- 'if' '(' expr ')' stmt
	else-if :- 'else' if-stmt-base
	if-stmt :- if-stmt-base else-if*
			| if-stmt-base else-if* 'else' stmt
	expr :- fcall | id | num | '(' expr ')' | expr op expr
	fcall :- id '(' expr (',' epxr)* ')'
	type :- int | float | type '[' expr ']' | type '[' ']' | type '*'
			| 'union' id | 'struct' id | struct-decl-base | union-decl-base
	var-decl :- type id ';' | type id '=' expr ';'
	fun-decl-base :- type id '(' ')' | type id '(' expr (',' expr)* ')'
	fun-decl :- fun-decl-base ';' | fun-decl-base '{' stmt* '}'
	type-decl-base :- id? '{' (type id ';')* '}' ';'
	union-decl-base :- 'union' type-decl-base
	struct-decl-base :- 'struct' type-decl-base
	type-decl :- union-decl-base ';' | struct decl-base ';'
*/


struct hash {
	size_t len;
	uint8_t* data;
};

enum stmt_type {
	STMT_IF,
	STMT_EXPR,
	STMT_VARDEC,
	STMT_BLOCK,
};

struct scope;

struct if_stmt {
	/* form linked list of if-else's, if condition == NULL, then it's an else */
	struct ast* else_chain;
	struct expr* condition;
	struct ast* stmt;
};

enum expr_type {
	EXPR_VAR,
	EXPR_NUM,
	EXPR_BINOP,
	EXPR_UNOP,
	EXPR_FCALL,
	EXPR_CAST,
	EXPR_IDX,
};

struct expr;

struct unop_expr {
	enum op op;
	struct expr* child;
};

struct idx_expr {
	struct expr* child;
	struct expr* idx;
};

struct binop_expr {
	enum op op;
	struct expr* left;
	struct expr* right;
};

struct fcall {
	struct symbol* sym;
	vec(struct expr*) args;
};

struct expr {
	enum expr_type expr_type;
	struct token* tok;
	struct type* type;
	struct hash* hash;
	union {
		struct symbol* sym;
		struct binop_expr binop;
		struct unop_expr unop;
		struct fcall fcall;
		struct expr* child;
		struct idx_expr idx;
		/* TODO: make a more complex type for numbers */
		int64_t num;
	};
};

struct block {
	struct ast* code;
	struct scope* scope;
};

struct vardec {
	struct symbol* sym;
	struct expr* initializer;
};

struct fundecl {
	struct symbol* sym;
	int is_complete;
};

enum ast_type {
	AST_STMT,
	AST_FUNDECL,
	AST_UNION_DECL,
	AST_STRUCT_DECL,
};

struct ast {
	struct ast* next;
	enum ast_type ast_type;
	enum stmt_type stmt_type;
	union {
		struct if_stmt if_stmt;
		struct expr* expr;
		struct block block_stmt;
		struct vardec vardec;
		struct fundecl fundecl;
	};
};

#endif
