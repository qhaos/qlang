#ifndef TYPE_PASS_H_
#define TYPE_PASS_H_

#include "ast.h"

void type_pass(struct ast* ast);

#endif
