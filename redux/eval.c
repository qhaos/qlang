#include "ast.h"
#include "types.h"
#include "print.h"

static struct expr* num(int64_t x, struct token* tok) {
	struct expr* ret = calloc(sizeof(struct expr), 1);

	ret->expr_type = EXPR_NUM;
	ret->num = x;
	ret->tok = tok;

	return ret;
}

struct expr* const_expr_eval(struct expr* expr);

int64_t const_numer_cast(int64_t num, struct type* type) {
	switch(type->keyword) {
		case KEY_U8:
			return (char)num;
		break;
		case KEY_U16:
			return (unsigned short)num;
		break;
		case KEY_U32:
			return (unsigned int)num;
		break;
		default:
			abort();
		break;
	}
}

static struct expr* const_unop_eval(struct expr* expr) {
	struct expr* child = const_expr_eval(expr->unop.child);
	if(!child)
		return NULL;
	if(child->expr_type != EXPR_NUM)
		return NULL;

	expr->expr_type = EXPR_NUM;
	expr->type = child->type;

	switch(expr->unop.op) {
		case OP_MINUS:
			expr->num = -child->num;
		break;
		case OP_ADD:
			expr->num = child->num;
		break;
		case OP_BNOT:
			expr->num = ~child->num;
		break;
		case OP_LNOT:
			expr->num = !child->num;
		break;
		default:
			return NULL;
		break;
	}

	expr->num = const_numer_cast(expr->num, expr->type);

	free(child);
	
	return expr;
}

static struct expr* const_binop_eval(struct expr* expr) {
	if(!expr)
		return NULL;
	struct expr* tmp;
	tmp = const_expr_eval(expr->binop.left);
	if(!tmp)
		return NULL;
	tmp = const_expr_eval(expr->binop.right);
	if(!tmp)
		return NULL;
	int64_t rhs = expr->binop.right->num;
	int64_t result = expr->binop.left->num;
	switch(expr->binop.op) {
		case OP_EQ:
			result = result == rhs;
		break;
		case OP_NOTEQ:
			result = result != rhs;
		break;
		case OP_LESS:
			result = result < rhs;
		break;
		case OP_MORE:
			result = result > rhs;
		break;
		case OP_LESSEQ:
			result = result <= rhs;
		break;
		case OP_MOREEQ:
			result = result >= rhs;
		break;
		case OP_ADD:
			result += rhs;
		break;
		case OP_MINUS:
			result += rhs;
		break;
		case OP_STAR:
			result *= rhs;
		break;
		case OP_DIV:
			if(!rhs) {
				print_error(expr->binop.right->tok, "division by zero");
				return NULL;
			}
			result /= rhs;
		break;
		case OP_MOD:
			if(!rhs) {
				print_error(expr->binop.right->tok, "division by zero");
				return NULL;
			}
			result %= rhs;
		break;
		case OP_LSHIFT:
			result >>= rhs;
		break;
		case OP_RSHIFT:
			result <<= rhs;
		break;
		case OP_LAND:
			result = result && rhs;
		break;
		case OP_BAND:
			result &= rhs;
		break;
		case OP_LOR:
			result = result || rhs;
		break;
		case OP_BOR:
			result |= rhs;
		break;
		case OP_XOR:
			result ^= rhs;
		break;
		default:
			return NULL;
		break;
	}
	free(expr->binop.left);
	free(expr->binop.right);
	expr->expr_type = EXPR_NUM;
	expr->num = result;
	return expr;
}


/* evaluate what *should* be a constant expression, otherwise return NULL */
struct expr* const_expr_eval(struct expr* expr) {
	if(!expr)
		return NULL;

	switch(expr->expr_type) {
		case EXPR_VAR:
			return NULL;
		case EXPR_NUM:
			return expr;
		case EXPR_BINOP:
			return const_binop_eval(expr);
		case EXPR_UNOP:
			return const_unop_eval(expr);
		case EXPR_FCALL:
			return NULL;
		case EXPR_CAST: {
			struct expr* ret = const_expr_eval(expr->child);
			if(!ret)
				return NULL;
			int64_t n = const_numer_cast(ret->num, expr->type);
			free(expr->child);
			expr->expr_type = EXPR_NUM;
			expr->num = n;
		}
		break;
		case EXPR_IDX:
			return NULL;
		break;
	}
}
