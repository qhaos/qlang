#ifndef VEC_H_
#define VEC_H_

#include <stdlib.h>

struct _vec {
    size_t len;
    size_t cap;
};

#define vec(T) T*

#define vec_new(T, CAP) ((T*)_vec_new(sizeof(T), (CAP)))
#define vec_len(v) ((((struct _vec*)v)[-1]).len)
#define vec_cap(v) ((((struct _vec*)v)[-1]).cap)
#define vec_recap(v, CAP) (_vec_recap((void**)&v, sizeof(*v), CAP))
#define vec_push(v, elem) (*(typeof(v))(_vec_push_helper((void**)&v, sizeof(*v))) = elem)
#define vec_back(v) (&v[vec_len(v)-1])
#define vec_pop(v) ((void)--vec_len(v))
#define vec_foreach(v, f) for(size_t _i = 0; _i < vec_len((v)); _i++) { (f)(v[_i]); }
#define vec_destroy(v) (void)(free(&vec_len(v)), v = NULL)

static void* _vec_new(size_t sz, size_t cap) {
    struct _vec* mem = calloc(sizeof(struct _vec) + sz*cap, 1);

    mem->len = 0;
    mem->cap = cap;

    return (char*)(mem+1);
}

static void* _vec_recap(void** _mem, size_t sz, size_t cap) {
    struct _vec* mem = realloc(((struct _vec*)*_mem)-1, sizeof(struct _vec) + sz*cap);
    mem->cap = cap;
    if(mem->cap < mem->len)
        mem->len = mem->cap;

    return (*_mem = (void*)(mem+1));
}

static void* _vec_push_helper(void** v, size_t sz) {
    size_t last = vec_len(*v)++;

    if(vec_cap(*v) == last)
        _vec_recap(v, sz, vec_cap(*v)+1);

    return (void*)((char*)(*v)+sz*last);
}

#endif
