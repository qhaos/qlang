#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "code.h"
#include "iden.h"
#include "lex.h"
#include "print.h"
#include "sym.h"
#include "types.h"


void print_tok(struct token* tok, int color) {
	fprintf(stderr, "[%s:%d:%d] ", tok->file->name, tok->row+1, tok->col+1);
	switch(tok->type) {
		case TOK_KEYWORD:
			fprintf(stderr, "keyword: %s\n", keywords[tok->keyword]);
		break;
		case TOK_NUM:
			fprintf(stderr, "number: %ld (0x%lX):\n", tok->num, tok->num);
		break;
		case TOK_IDEN: {
			struct iden* id = &identbl[tok->iden];
			fprintf(stderr, "iden: { .hash = 0x%lX, .data = \"%s\" (%p), .len = %zu }:\n",
							id->hash, id->data, id->data, id->len);
		}
		break;
		case TOK_OP: {
			fprintf(stderr, "operator: %s (#%d):\n", ops[tok->op], tok->op);
		}
		break;
		default: {
			fprintf(stderr, "%c (0x%X):\n", tok->type, tok->type);
		}
		break;
	}

	fprintf(stderr, "\t%4d | ", tok->row+1);
	fwrite(tok->file->lines[tok->row], 1, tok->col, stderr);
	fprintf(stderr, "\033[%dm", color);
	fwrite(tok->file->lines[tok->row]+tok->col, 1, tok->len, stderr);
	fprintf(stderr, "\033[m%s", tok->file->lines[tok->row]+tok->col+tok->len);

	fprintf(stderr, "\t     | ");
	for(int i = 0; i < tok->col; i++) {
		if(tok->file->lines[tok->row][i] == '\t') {
			putc('\t', stderr);
		} else {
			putc(' ', stderr);
		}
	}
	fprintf(stderr, "\033[%dm^", color);
	for(size_t i = 1; i < tok->len; i++)
		putc('~', stderr);
	fputs("\033[m\n", stderr);
}

void print_error(struct token* tok, const char* msg, ...) {
	va_list v;
	va_start(v, msg);
	fputs("error: \033[31m", stderr);
	vfprintf(stderr, msg, v);
	fputs("\033[m\n", stderr);
	print_tok(tok, 31);
	va_end(v);
	exit(1);
}

void print_type(struct type* type) {
	switch(type->type_type) {
		case TYPE_BUILTIN:
			fprintf(stderr, "%s", keywords[type->keyword]);
		break;
		case TYPE_ENUM:
		case TYPE_UNION:
		case TYPE_STRUCT:
			fprintf(stderr, "%s", identbl[type->sym->iden].data);
		break;
		case TYPE_PTR:
			print_type(type->child);
			putc('*', stderr);
		break;
		case TYPE_ARRAY:
			print_type(type->child);
			fputs("[]", stderr);
		break;
	}
}

static char* operation_strings[] = {
	"add",
	"sub",
	"mul",
	"div",
	"shl",
	"shr",
	"and",
	"or",
	"xor",
	"push",
	"pop",
	"mov",
	"data",
	"setz",
	"setnz",
	"not",
	"neg",
	"test"
};



void print_code() {
	for(size_t i = 0; i < program_len; i++) {
		struct instruction* in = program + i;
		fprintf(stderr, "%s ", operation_strings[in->op]);
		if(in->dst.operand_type & OPERAND_REGISTER)
			fprintf(stderr, "r%d", in->dst.reg);
		else if(in->dst.operand_type & OPERAND_ADDRESS && !(in->dst.operand_type & OPERAND_SYMBOL))
			fprintf(stderr, "[%lu]", in->dst.val);
		else if(in->dst.operand_type & OPERAND_IMM)
			fprintf(stderr, "[%lu]", in->dst.val);
		else
			fprintf(stderr, "[%s]", identbl[in->dst.sym->iden].data);

		if(in->op == OPER_PUSH || in->op == OPER_POP || in->op == OPER_NEG || in->op == OPER_NOT) {
			fprintf(stderr, "\n");
			continue;
		}
		fprintf(stderr, ", ");

		if(in->src.operand_type & OPERAND_REGISTER)
			fprintf(stderr, "r%d\n", in->src.reg);
		else if(in->src.operand_type & OPERAND_ADDRESS && !(in->src.operand_type & OPERAND_SYMBOL))
			fprintf(stderr, "[%lu]\n", in->src.val);
		else if(in->src.operand_type & OPERAND_IMM)
			fprintf(stderr, "[%lu]\n", in->src.val);
		else
			fprintf(stderr, "[%s]\n", identbl[in->src.sym->iden].data);
	}
}
