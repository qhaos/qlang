#include "ast.h"
#include "eval.h"
#include "print.h"
#include "sym.h"

static bool is_numerical(struct type* type) {
	return ((type->type_type == TYPE_BUILTIN && type->keyword != KEY_VOID)
		|| type->type_type == TYPE_ENUM
		|| type->type_type == TYPE_PTR);
}

static bool compat_types(struct type* left, struct type* right) {
	if(type_cmp(left, right))
		return true;

	if(is_numerical(left))
		return is_numerical(right);

	return false;
}

static struct type* type_max(struct type* left, struct type* right) {
	if(!compat_types(left, right))
		return NULL;
	if(type_cmp(left, right))
		return left;

	if(is_numerical(left)) {
		if(left->type_type == TYPE_ENUM)
			return left;
		if(right->keyword > left->keyword)
			return right;
		return left;
	}
}

size_t type_sizeof(struct type* type) {
	if(type->width != (size_t)-1)
		return type->width;
	switch(type->type_type) {
		case TYPE_BUILTIN:
			type->width = 1 << ((type->keyword+1) >> 1);
		break;
		case TYPE_ENUM:
			 return 8;
		break;
		case TYPE_UNION:
		break;
		case TYPE_STRUCT:
		break;
		case TYPE_PTR:
			return 8;
		break;
		case TYPE_ARRAY:
			struct expr* size = const_expr_eval(type->len);
			if(!size) {
				print_error(type->len->tok, "constant numerical expression required for array size");
				return -1;
			}
			type->len = size;
			type->width = size->num * type_sizeof(type->child);
		break;
	}
	return type->width;
}

static void assign_type(struct expr* expr) {
	switch (expr->expr_type) {
		case EXPR_IDX:
			expr->type = expr->idx.child->type->child;
		break;
		case EXPR_FCALL: {
			expr->type = expr->fcall.sym->fun.ret_type;
			vec(struct expr*) args = expr->fcall.args;;
			for(size_t i = 0; i < vec_len(args); i++) {
				assign_type(args[i]);
				if(!compat_types(args[i]->type, expr->fcall.sym->fun.args[i]->type)) {
					print_error(args[i]->tok, "type mismatch in arguement %zu", i);
				}
			}
		} break;
		case EXPR_CAST:
			if(!compat_types(expr->type, expr->child->type))
				print_error(expr->tok, "cast has incompatible types");
		break;
		case EXPR_UNOP:
			assign_type(expr->unop.child);
			if(expr->unop.op == OP_LNOT) {
				expr->type = builtin(KEY_U8);
			} else if(expr->unop.op == OP_STAR) {
				if(expr->unop.child->type->type_type != TYPE_PTR)	
					print_error(expr->tok, "object of derefrencing must be a pointer type");
				expr->type = expr->unop.child->type->child;
			} else if(expr->unop.op == OP_BAND) {
				expr->type = ptr(expr->unop.child->type);
			} else {
				expr->type = expr->unop.child->type;
				if(!is_numerical(expr->type)) {
					print_error(expr->tok, "unary operators other than & can only be applied to numerical types");
				}
			}
		break;
		case EXPR_VAR:
			expr->type = expr->sym->type;
		break;
		case EXPR_NUM:
			expr->type = builtin(KEY_I64);
		break;
		case EXPR_BINOP:
			assign_type(expr->binop.left);
			assign_type(expr->binop.right);
			if(!(is_numerical(expr->binop.left->type) && is_numerical(expr->binop.right->type))) {
				print_error(expr->tok, "operands to binary operators must be numerical");
			}
			expr->type = type_max(expr->binop.left->type, expr->binop.right->type);
			if(!expr->type)
				print_error(expr->tok, "incompatible types for %s operator", ops[expr->binop.op]);
			if(expr->binop.op == OP_EQ
				|| expr->binop.op == OP_NOTEQ
				|| expr->binop.op == OP_MORE
				|| expr->binop.op == OP_MOREEQ
				|| expr->binop.op == OP_LESS
				|| expr->binop.op == OP_LESSEQ
				|| expr->binop.op == OP_LAND
				|| expr->binop.op == OP_LOR)
			switch(expr->binop.op) {
				case OP_EQ:
				case OP_NOTEQ:
				case OP_MORE:
				case OP_MOREEQ:
				case OP_LESS:
				case OP_LESSEQ:
				case OP_LAND:
				case OP_LOR:
					expr->type = builtin(KEY_U8);
				break;
				case OP_ASSGN:
				case OP_ADDASSGN:
				case OP_SUBASSGN:
				case OP_MULASSGN:
				case OP_DIVASSGN:
				case OP_MODASSGN:
				case OP_LSHIFTASSGN:
				case OP_RSHIFTASSGN:
				case OP_BANDASSGN:
				case OP_BORASSGN:
				case OP_XORASSGN:
					if(!(	expr->binop.left->expr_type == EXPR_VAR
						||	expr->binop.left->expr_type == EXPR_IDX
						||	(expr->binop.left->expr_type == EXPR_UNOP && expr->binop.left->unop.op == OP_STAR)
					)) {
						print_error(expr->tok, "left hand side of assignment expression is not a valid left hand side");
					}
				break;
				default:
				break;
			}
		break;
	}
}

void type_pass(struct ast* ast);

static void type_stmt(struct ast* ast) {
	if(!ast)
		return;
	switch(ast->stmt_type) {
		case STMT_IF:
			assign_type(ast->if_stmt.condition);
			type_pass(ast->if_stmt.stmt);
			type_pass(ast->if_stmt.else_chain);
		break;
		case STMT_EXPR:
			assign_type(ast->expr);
		break;
		case STMT_VARDEC:
		break;
		case STMT_BLOCK:
			type_stmt(ast->block_stmt.code);
		break;
	}
	type_stmt(ast->next);
}

void type_pass(struct ast* ast) {
	if(!ast)
		return;

	switch(ast->ast_type) {
		case AST_STMT:
			type_stmt(ast);
		break;
		case AST_FUNDECL:
			type_stmt(ast->fundecl.sym->fun.code);
		break;
		default:
		break;
	}

	type_pass(ast->next);
}
