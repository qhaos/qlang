#ifndef LISP_H_
#define LISP_H_

struct ast;

void lispify(struct ast* head);

#endif
