#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "ast.h"
#include "eval.h"
#include "print.h"
#include "sym.h"

static void hash_append(struct hash* hash, uint8_t byte) {
	hash->data = realloc(hash->data, ++hash->len);
	hash->data[hash->len-1] = byte;
}

static void hash_expr(struct hash* parent, struct expr* expr) {
	struct hash* new = calloc(sizeof(struct hash), 1);
	switch(expr->expr_type) {
		case EXPR_NUM:
			hash_append(new, 0b00000000 | (expr->num & 0b1111)); 
		break;
		case EXPR_VAR:
			hash_append(new, 0b00100000 | (expr->sym->iden & 0b1111)); 
		break;
		case EXPR_UNOP:
			/* thank god there are exactly 31 operators lol */
			hash_append(new, 0b01000000 | expr->unop.op);
			hash_expr(new, expr->unop.child);
		break;
		case EXPR_BINOP:
			hash_append(new, 0b01100000 | expr->binop.op);
			hash_expr(new, expr->binop.left);
			hash_expr(new, expr->binop.right);
		break;
		case EXPR_FCALL:
			hash_append(new, 0b10000000 | (expr->fcall.sym->iden & 0b11111));
		break;
		case EXPR_CAST:
			hash_append(new, 0b10100000);
			hash_expr(new, expr->child);
		break;
		case EXPR_IDX:
			hash_append(new, 0b11100000);
			hash_expr(new, expr->idx.child);
			hash_expr(new, expr->idx.idx);
		break;
	}
	if(parent) {
		parent->data = realloc(parent->data, parent->len + new->len);
		memcpy(parent->data + parent->len, new->data, new->len);
		parent->len += new->len;
	}
	expr->hash = new;
}

static struct hash* hash(struct expr* expr) {
	hash_expr(NULL, expr);
	return expr->hash;
}

/* -1 on less, +1 on more, 0 on equal */
static int hash_cmp(struct hash* lhs, struct hash* rhs) {
	if(lhs->len > rhs->len)
		return 1;
	else if(lhs->len < rhs->len)
		return -1;

	return strncmp((char*)lhs->data, (char*)rhs->data, lhs->len);
}

bool is_commutative[] = {
	true,
	false,
	true,
	false,
	false,
	true,
	false,
	false,
	false,
	true,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	false,
	false,
	true,
	false,
	true,
};

static void swap_exprs(struct expr* lhs, struct expr* rhs) {
	struct expr tmp = *lhs;
	*lhs = *rhs;
	*rhs = tmp;
}

struct expr* sort(struct expr* expr) {
	const_expr_eval(expr);
	switch(expr->expr_type) {
		case EXPR_BINOP:
			const_expr_eval(expr->binop.left);
			sort(expr->binop.left);
			const_expr_eval(expr->binop.right);
			sort(expr->binop.right);
			hash(expr->binop.left);
			hash(expr->binop.right);
			if(hash_cmp(expr->binop.left->hash, expr->binop.right->hash) > 0) {
				if(is_commutative[expr->binop.op])
					swap_exprs(expr->binop.left, expr->binop.right);
			}
			hash(expr);
		break;
		case EXPR_UNOP:
			const_expr_eval(expr);
			sort(expr->unop.child);
			hash(expr);
		break;
		case EXPR_FCALL:
		break;
		case EXPR_CAST: {
			struct expr* ret = const_expr_eval(expr->child);
			if(ret) {
				int64_t n = const_numer_cast(ret->num, expr->type);
				free(expr->child);
				expr->expr_type = EXPR_NUM;
				expr->num = n;
			} else {
				sort(expr->child);
			}
			hash(expr);
		}
		break;
		case EXPR_IDX:
			sort(expr->idx.child);
			sort(expr->idx.idx);
		break;
		case EXPR_NUM:
		case EXPR_VAR:
		break;
	}
	return expr;
}

void sort_pass(struct ast* ast);

static void sort_stmt(struct ast* ast) {
	if(!ast)
		return;
	switch(ast->stmt_type) {
		case STMT_IF:
			sort(ast->if_stmt.condition);
			sort_pass(ast->if_stmt.stmt);
			sort_pass(ast->if_stmt.else_chain);
		break;
		case STMT_EXPR:
			sort(ast->expr);
		break;
		case STMT_VARDEC:
		break;
		case STMT_BLOCK:
			sort_stmt(ast->block_stmt.code);
		break;
	}
	sort_stmt(ast->next);
}

void sort_pass(struct ast* ast) {
	if(!ast)
		return;

	switch(ast->ast_type) {
		case AST_STMT:
			sort_stmt(ast);
		break;
		case AST_FUNDECL:
			sort_stmt(ast->fundecl.sym->fun.code);
		break;
		default:
		break;
	}

	sort_pass(ast->next);	
}
