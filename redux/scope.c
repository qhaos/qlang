#include <stdlib.h>

#include "iden.h"
#include "print.h"
#include "scope.h"
#include "sym.h"
#include "types.h"

struct scope global = {
	.parent = NULL,
	.symbols = {NULL},
	.children = NULL
};

struct scope* scope(struct scope* parent) {
	struct scope* ret = calloc(sizeof(struct scope), 1);

	ret->parent = parent;
	ret->children = vec_new(struct scope*, 0);

	if(parent != &global) {
		vec_push(parent->children, ret);
	}

	return ret;
}

struct symbol* scope_find(struct scope* scope, size_t iden) {
	if(!scope)
		return NULL;
	size_t hash = iden & 0xF;

	for(struct symbol* sym = scope->symbols[hash]; sym; sym = sym->next) {
		if(sym->iden == iden)
			return sym;
	}

	return scope_find(scope->parent, iden);
}

struct symbol* scope_add(struct scope* scope, struct symbol* sym, bool* changed) {
	sym->is_local = (scope != &global);
	size_t hash = sym->iden & 0xF;

	struct symbol** tail = &scope->symbols[hash];

	for(struct symbol* i = scope->symbols[hash]; i; i = i->next) {
		tail = &i->next;
		if(i->iden == sym->iden) {
			if(i->symbol_type == SYM_FUNCTION && sym->symbol_type == SYM_FUNCTION) {
				if(!type_cmp(i->fun.ret_type, sym->fun.ret_type)) {
					print_error(sym->defined, "return type mismatch");
					return NULL;
				}
				if(vec_len(sym->fun.args) != vec_len(i->fun.args)) {
					print_error(
						sym->defined,
						"%s has %zu arguements instead of %zu (as previously declared)",
						identbl[sym->iden].data, vec_len(sym->fun.args), vec_len(i->fun.args)
					);
					return NULL;
				}
				for(size_t j = 0; j < vec_len(sym->fun.args); j++) {
					if(!type_cmp(sym->fun.args[j]->type, i->fun.args[j]->type)) {
						print_error(
							sym->defined,
							"%s has arguement type mismatch at arguement %zu",
							identbl[sym->iden].data, j
						);
						return NULL;
					}
				}
				if(i->fun.code) {
					*changed = true;
					*i = *sym;
					i->next = *tail;
				} else {
					*changed = false;
				}
				return i;
			}
			return NULL;
		}
	}

	*tail = sym;
	return sym;
}

