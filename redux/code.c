#include <stdlib.h>

#include "ast.h"
#include "code.h"
#include "print.h"
#include "scope.h"
#include "sym.h"
#include "vec.h"

static void (*binop_func_table[]) (struct expr*, int);

enum {
	RAX,
	RBX,
	RCX,
	RDX,
	RBP,
	RSI,
	RDI,
	RSP,
	R8,
	R9,
	R10,
	R11,
	R12,
	R13,
	R14,
	R15,
};

size_t addr = START_ADDR;

struct instruction* program = NULL;
size_t program_len = 0;

static void add_instruction(struct instruction* in) {
	program = realloc(program, (program_len+1) * sizeof(struct instruction));
	program[program_len++] = *in;
}

static struct instruction* instruction(enum operation op) {
	struct instruction* ret = calloc(sizeof(struct instruction), 1);
	ret->op = op;
	ret->address = addr;
	ret->size = 8;
	return ret;
}

static int reg_usage[] = {
	0, 0, 0, 0, 5, 0, 0, 99,
	0, 0, 0, 0, 0, 0, 0, 0,
};

static int select_register(int reg) {
	if(reg != -1) {
		if(reg_usage[reg]++) {
			struct instruction push = (struct instruction) {
				.op = OPER_PUSH,
				.dst = (struct operand) {
					.operand_type = OPERAND_REGISTER,
					.reg = reg,
				},
				.instruction_size = reg > 7? 2 : 1,
				.address = addr,
				.size = 8,
			};
			add_instruction(&push);
		}
		return reg;
	}

	reg = 0;

	for(int i = 1; i < 16; i++) {
		if(!reg_usage[reg])
			break;
		if(reg_usage[i] < reg_usage[reg])
			reg = i;
	}

	if(reg_usage[reg]++) {
		struct instruction push = (struct instruction) {
			.op = OPER_PUSH,
			.dst = (struct operand) {
				.operand_type = OPERAND_REGISTER,
				.reg = reg,
			},
			.instruction_size = reg > 7? 2 : 1,
			.address = addr,
			.size = 8,
		};
		add_instruction(&push);
	}

	return reg;
}

static void deselect_register(int reg) {
	if(reg == -1)
		return;
	reg_usage[reg]--;
	if(reg_usage[reg]) {
		struct instruction push = (struct instruction) {
			.op = OPER_POP,
			.dst = (struct operand) {
				.operand_type = OPERAND_REGISTER,
				.reg = reg,
			},
			.instruction_size = reg > 7? 2 : 1,
			.address = addr,
			.size = 8,
		};
		add_instruction(&push);
	}
}

static void codegen_expr(struct expr* expr, int reg);

/* returns -1 if register not allocated, otherwise return register */
static int gen_src(struct operand* op, struct expr* expr, int address_allowed) {
	int reg = -1;

	switch(expr->expr_type) {
		case EXPR_VAR: {
			*op = (struct operand) {
				.operand_type = OPERAND_SYMBOL,
				.sym = expr->sym,
			};
		}
		break;
		case EXPR_NUM:
			*op = (struct operand) {
				.operand_type = OPERAND_IMM,
				.val = expr->num,
			};
		break;
		case EXPR_UNOP:
			if(expr->unop.op == OP_BAND) {
				if(expr->unop.child->expr_type != EXPR_VAR) {
					print_error(expr->tok, "cannot find address of expression");
				}
				if(address_allowed) {
					*op = (struct operand) {
						.operand_type = OPERAND_SYMBOL,
						.sym = expr->unop.child->sym,
					};
				} else {
					reg = select_register(reg);
					struct instruction in = (struct instruction) {
						.op = OPER_MOV,
						.dst = {
							.operand_type = OPERAND_REGISTER,
							.reg = reg,
						},
						.src = {
							.operand_type = OPERAND_SYMBOL,
							.sym = expr->unop.child->sym,
						},
					};
					add_instruction(&in);
					*op = (struct operand) {
						.operand_type = OPERAND_REGISTER,
						.reg = reg,
					};
				}
				break;
			}
		default:
			reg = select_register(-1);
			codegen_expr(expr, reg);
			*op = (struct operand) {
				.operand_type = OPERAND_REGISTER,
				.reg = reg,
			};
		break;
	}

	return reg;
}

static void codegen_binop(struct expr* expr, int reg) {
	binop_func_table[expr->binop.op](expr, reg);
	
	return;
}

static void codegen_unop(struct expr* expr, int reg) {
	struct expr* child = expr->unop.child;
	/* handle !!x idiom */
	if(expr->unop.op == OP_LNOT && expr->unop.child->expr_type == EXPR_UNOP && expr->unop.child->unop.op == OP_LNOT) {
		codegen_expr(child->unop.child, reg);
		struct instruction in = {
			OPER_TEST,
			.dst = (struct operand) {
			  OPERAND_REGISTER,
			  {reg}
			},
			.src = (struct operand) {
			  OPERAND_REGISTER,
			  {reg}
			}
		};
		add_instruction(&in);
		in.op = OPER_SETNZ;
		add_instruction(&in);
		return;
	}
	codegen_expr(expr->unop.child, reg);
	struct instruction in;
	in.dst = (struct operand) {
		OPERAND_REGISTER,
		{reg}
	};

	switch(expr->unop.op) {
		case OP_MINUS:
			in.op = OPER_NEG;
		break;
		case OP_BNOT:
			in.op = OPER_NOT;
		case OP_LNOT:
			in.op = OPER_TEST;
			in.src = in.dst;
			add_instruction(&in);
			in.op = OPER_SETNZ;
		break;
		case OP_BAND:
			abort();
		default:
		break;
	}
	add_instruction(&in);
}

static void codegen_expr(struct expr* expr, int reg) {
	/*
	 *  EXPR_VAR   -> mov reg, [address]
	 *  EXPR_NUM   -> mov 
	 *  EXPR_BINOP -> TODO
	 *  EXPR_FCALL -> TODO
	 *  EXPR_CAST  -> TODO
	 *  EXPR_IDX   -> TODO
	 * 
	 */

	switch(expr->expr_type) {
		case EXPR_VAR: {
			struct instruction in = (struct instruction) {
				.op = OPER_MOV,
				.dst = {
					.operand_type = OPERAND_REGISTER,
					.reg = reg,
				},
				.src = {
					.operand_type = OPERAND_SYMBOL,
					.sym = expr->sym,
				},
			};

			add_instruction(&in);
		}
		break;
		case EXPR_NUM: {
			struct instruction in = (struct instruction) {
					.op = OPER_MOV,
					.dst = {
						.operand_type = OPERAND_REGISTER,
						.reg = reg,
					},
					.src = {
						.operand_type = OPERAND_IMM,
						.val = expr->num,
					},
				};
	
				add_instruction(&in);
			}
		break;
		case EXPR_BINOP:
			codegen_binop(expr, reg);
		break;
		case EXPR_UNOP:
			codegen_unop(expr, reg);
		break;
	}
}

static size_t max(size_t l, size_t r) {
	if(l < r)
		return r;
	else
		return l;
}

static size_t assign_offsets(struct scope* scope, size_t offset) {
	for(size_t i = 0; i < 16; i++) {
		for(struct symbol* head = scope->symbols[i]; head; head = head->next) {
			head->offset = offset;
			offset += head->type->width;
		}
	}

	for(size_t i = 0; i < vec_len(scope->children); i++) {
		offset = max(assign_offsets(scope->children[i], offset), offset);
	}

	return offset;
}

void codegen(struct ast* ast);

void codegen_fundecl(struct ast* ast) {
	struct symbol* sym = ast->fundecl.sym;
	struct scope* scope = sym->fun.scope;
	size_t offset = assign_offsets(scope, 0);
	struct instruction* ret =  NULL;
	/* round it off to the nearest multiple of 16 */
	if(offset & 0xF) {
		offset >>= 4;
		offset += 1;
		offset <<= 4;
	}

	if(offset) {
		struct instruction push = (struct instruction) {
			.op = OPER_SUB,
			.dst = (struct operand) {
				.operand_type = OPERAND_REGISTER,
				.reg = RSP,
			},
			.src = (struct operand) {
				.operand_type = OPERAND_IMM,
				.val = offset,
			},
			.instruction_size = 4,
			.address = addr,
			.size = 8,
		};
		add_instruction(&push);
		ret = instruction(OPER_SUB);
		ret->dst = (struct operand) {
			OPERAND_REGISTER,
			.reg = RSP,
		};
		ret->src = (struct operand) {
			OPERAND_IMM,
			.val = offset,
		};
		codegen(ast->fundecl.sym->fun.code);
		add_instruction(ret);
	}

}

void codegen(struct ast* ast) { 
	if(!ast)
		return;

	int reg;
	switch(ast->ast_type) {
		case AST_STMT:
			switch(ast->stmt_type) {
				case STMT_EXPR:
					reg = select_register(-1);
					codegen_expr(ast->expr, reg);
					deselect_register(reg);
				break;
				case STMT_IF:
				break;
				default:
				break;
			}
		break;
		case AST_FUNDECL:
			if(ast->fundecl.is_complete) {
				codegen_fundecl(ast);
			}
		break;
		default:
		break;
	}

	codegen(ast->next);
}

static void codegen_op_eq(struct expr* expr, int reg) {
	/* reg  <- expr.lhs
	 * reg1 <- expr.rhs
	 * sub reg, reg1
	 * setz reg
	 * and reg, 0x1 ; implied by OPER_SETE
	 */
	codegen_expr(expr->binop.left, reg);
	int reg1 = select_register(-1);
	codegen_expr(expr->binop.right, reg1);

	struct instruction in = (struct instruction) {
		.op = OPER_SUB,
		.dst = (struct operand) {
			OPERAND_REGISTER,
			{ reg }
		},
		.src = (struct operand) {
			OPERAND_REGISTER,
			{ reg1 }
		},
	};
	add_instruction(&in);
	in.op = OPER_SETZ;
	add_instruction(&in);
	deselect_register(reg1);
}

static void codegen_op_noteq(struct expr* expr, int reg) {
	/* really, really hacky */
	codegen_op_eq(expr, reg);
	program[program_len-1].op = OPER_SETNZ;
}

static void codegen_assgn_helper(struct expr* expr, int reg) {
	struct expr* lhs = expr->binop.left;
	/* checking was already done in the type pass as to wether or not its a valid lhs */
	struct operand dst;
	switch(lhs->expr_type) {
		case EXPR_VAR:
			dst.operand_type = OPERAND_SYMBOL;
			dst.sym = lhs->sym;
			
		break;
		case EXPR_IDX:
			print_error(lhs->tok, "INDICES NOT YET IMPLEMENTED, ONE STEP AT A TIME\n");
		break;
		case EXPR_UNOP: {
			dst.reg = select_register(-1);
			codegen_expr(lhs->unop.child, dst.reg);
			dst.operand_type = OPERAND_REGISTER | OPERAND_ADDRESS;
			deselect_register(dst.reg);
		} break;
		default:
			print_error(lhs->tok, "Not a valid LHS for an assignment, did you run the type pass like you should've?\n");
		break;
	}
	
	struct instruction in = (struct instruction) {
		.op = OPER_MOV,
		.dst = dst,
		.src = (struct operand) {
 			.operand_type = OPERAND_REGISTER,
 			.reg = reg,
		}
	};
	
	add_instruction(&in);
}

static void codegen_op_assgn(struct expr* expr, int reg) {
	/*
	 * reg <- rhs
	 * [lhs] <- reg
	 */

	codegen_expr(expr->binop.right, reg);
	codegen_assgn_helper(expr, reg);
}

static void codegen_op_add(struct expr* expr, int reg) {
	codegen_expr(expr->binop.left, reg);
	struct operand src;
	int r = gen_src(&src, expr->binop.right, 0);

	struct instruction in = (struct instruction) {
		.op = OPER_ADD,
		.dst = (struct operand) {
			.operand_type = OPERAND_REGISTER,
			.reg = reg,
		},
		.src = src
	};

	add_instruction(&in);
	deselect_register(r);
}

static void codegen_op_sub(struct expr* expr, int reg) {
	codegen_expr(expr->binop.left, reg);
	struct operand src;
	int r = gen_src(&src, expr->binop.right, 0);

	struct instruction in = (struct instruction) {
		.op = OPER_SUB,
		.dst = (struct operand) {
			.operand_type = OPERAND_REGISTER,
			.reg = reg,
		},
		.src = src
	};
	add_instruction(&in);
	deselect_register(r);
}

static void codegen_op_mul(struct expr* expr, int reg) {
	abort();
}

static void codegen_op_div(struct expr* expr, int reg) {
	abort();
}

static void codegen_op_mod(struct expr* expr, int reg) {
	abort();
}

static void codegen_op_lshift(struct expr* expr, int reg) {
	codegen_expr(expr->binop.left, reg);
	struct operand src;
	int r = gen_src(&src, expr->binop.right, 0);

	struct instruction in = (struct instruction) {
		.op = OPER_SHL,
		.dst = (struct operand) {
			.operand_type = OPERAND_REGISTER,
			.reg = reg,
		},
		.src = src
	};
	add_instruction(&in);
	deselect_register(r);
}

static void codegen_op_rshift(struct expr* expr, int reg) {

	codegen_expr(expr->binop.left, reg);
	struct operand src;
	int r = gen_src(&src, expr->binop.right, 0);

	struct instruction in = (struct instruction) {
		.op = OPER_SHR,
		.dst = (struct operand) {
			.operand_type = OPERAND_REGISTER,
			.reg = reg,
		},
		.src = src
	};
	add_instruction(&in);
	deselect_register(r);
}

static void codegen_op_moreeq(struct expr* expr, int reg) {
	abort();
}

static void codegen_op_lesseq(struct expr* expr, int reg) {
	abort();
}

static void codegen_op_less(struct expr* expr, int reg) {
	abort();
}

static void codegen_op_more(struct expr* expr, int reg) {
	abort();
}


static void codegen_op_land(struct expr* expr, int reg) {
	abort();
}

static void codegen_op_band(struct expr* expr, int reg) {
	codegen_expr(expr->binop.left, reg);
	struct operand src;
	int r = gen_src(&src, expr->binop.right, 0);

	struct instruction in = (struct instruction) {
		.op = OPER_AND,
		.dst = (struct operand) {
			.operand_type = OPERAND_REGISTER,
			.reg = reg,
		},
		.src = src
	};
	add_instruction(&in);
	deselect_register(r);
}

static void codegen_op_lor(struct expr* expr, int reg) {
	abort();
}

static void codegen_op_bor(struct expr* expr, int reg) {
	codegen_expr(expr->binop.left, reg);
	int reg1 = select_register(-1);
	codegen_expr(expr->binop.right, reg1);

	struct instruction in = (struct instruction) {
		.op = OPER_OR,
		.dst = (struct operand) {
			.operand_type = OPERAND_REGISTER,
			.reg = reg,
		},
		.src = (struct operand) {
			.operand_type = OPERAND_REGISTER,
			.reg = reg1,
		}
	};
	add_instruction(&in);
}

static void codegen_op_xor(struct expr* expr, int reg) {
	codegen_expr(expr->binop.left, reg);
	int reg1 = select_register(-1);
	codegen_expr(expr->binop.right, reg1);

	struct instruction in = (struct instruction) {
		.op = OPER_XOR,
		.dst = (struct operand) {
			.operand_type = OPERAND_REGISTER,
			.reg = reg,
		},
		.src = (struct operand) {
			.operand_type = OPERAND_REGISTER,
			.reg = reg1,
		}
	};
	add_instruction(&in);
}

static void codegen_op_addassgn(struct expr* expr, int reg) {
	codegen_op_add(expr, reg);

	codegen_assgn_helper(expr, reg);
}

static void codegen_op_subassgn(struct expr* expr, int reg) {
	codegen_op_sub(expr, reg);

	codegen_assgn_helper(expr, reg);
}

static void codegen_op_mulassgn(struct expr* expr, int reg) {
	codegen_op_mul(expr, reg);

	codegen_assgn_helper(expr, reg);
}

static void codegen_op_divassgn(struct expr* expr, int reg) {
	codegen_op_div(expr, reg);

	codegen_assgn_helper(expr, reg);
}

static void codegen_op_modassgn(struct expr* expr, int reg) {
	codegen_op_mod(expr, reg);

	codegen_assgn_helper(expr, reg);
}

static void codegen_op_lshiftassgn(struct expr* expr, int reg) {
	codegen_op_lshift(expr, reg);

	codegen_assgn_helper(expr, reg);
}

static void codegen_op_rshiftassgn(struct expr* expr, int reg) {
	codegen_op_rshift(expr, reg);

	codegen_assgn_helper(expr, reg);
}

static void codegen_op_bandassgn(struct expr* expr, int reg) {
	codegen_op_band(expr, reg);

	codegen_assgn_helper(expr, reg);
}

static void codegen_op_borassgn(struct expr* expr, int reg) {
	codegen_op_bor(expr, reg);

	codegen_assgn_helper(expr, reg);
}

static void codegen_op_xorassgn(struct expr* expr, int reg) {
	codegen_op_xor(expr, reg);

	codegen_assgn_helper(expr, reg);
}

static void codegen_op_bnot(struct expr* expr, int reg) {
	/* NOT A BINOP */
	(void) reg;
	print_error(expr->tok, "operator is not a binop, but was treated as such\n");
	return;
}


static void codegen_op_lnot(struct expr* expr, int reg) {
	/* NOT A BINOP */
	(void) reg;
	print_error(expr->tok, "operator is not a binop, but was treated as such\n");
	return;
}


static void (*binop_func_table[]) (struct expr*, int) = {
	codegen_op_eq,
	codegen_op_assgn,
	codegen_op_noteq,
	codegen_op_lnot,
	codegen_op_addassgn,
	codegen_op_add,
	codegen_op_subassgn,
	codegen_op_sub,
	codegen_op_mulassgn,
	codegen_op_mul,
	codegen_op_divassgn,
	codegen_op_div,
	codegen_op_modassgn,
	codegen_op_mod,
	codegen_op_lshiftassgn,
	codegen_op_lshift,
	codegen_op_rshiftassgn,
	codegen_op_rshift,
	codegen_op_lesseq,
	codegen_op_moreeq,
	codegen_op_less,
	codegen_op_more,
	codegen_op_bnot,
	codegen_op_land,
	codegen_op_bandassgn,
	codegen_op_band,
	codegen_op_lor,
	codegen_op_borassgn,
	codegen_op_bor,
	codegen_op_xorassgn,
	codegen_op_xor
};
