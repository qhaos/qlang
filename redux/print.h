#ifndef PRINT_H_
#define PRINT_H_

struct token;
struct type;
struct hash;
void print_tok(struct token* tok, int col);
void print_error(struct token* tok, const char* msg, ...);
void print_type(struct type* type);
void print_code();

#endif
