#include <stdlib.h>

#include "file.h"
#include "lex.h"

/* read in file */
struct file* readfile(FILE* in, const char* filename) {
	struct file* ret = calloc(sizeof(struct file), 1);

	ret->name = filename;

	while(1) {
		size_t n = 0;
		int ch = getc(in);
		if(ch == EOF)
			break;
		ungetc(ch, in);

		ret->rows++;
		ret->lines = realloc(ret->lines, sizeof(char*) * ret->rows);
		ret->lines[ret->rows-1] = NULL;
		getline(&ret->lines[ret->rows-1], &n, in);
	}

	ret->tokens = NULL;
	ret->tail = &ret->tokens;

	return ret;
}
