#ifndef SCOPE_H_
#define SCOPE_H_

#include <stdbool.h>
#include <stdint.h>

#include "vec.h"

struct scope {
	struct scope* parent;
	struct symbol* symbols[16];
	vec(struct scope*) children;
};

extern struct scope global;

struct scope* scope(struct scope* parent);
struct symbol* scope_find(struct scope* scope, size_t iden);
struct symbol* scope_add(struct scope* scope, struct symbol* sym, bool*);

#endif
