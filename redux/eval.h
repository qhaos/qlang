#ifndef EVAL_H_
#define EVAL_H_

#include <stdint.h>

#include "types.h"

int64_t const_numer_cast(int64_t num, struct type* type);
struct expr* const_expr_eval(struct expr* expr);

#endif
