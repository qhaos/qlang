#ifndef TYPES_H
#define TYPES_H

#include <stdbool.h>

#include "lex.h"

enum type_type {
	TYPE_BUILTIN,
	TYPE_ENUM,
	TYPE_UNION,
	TYPE_STRUCT,
	TYPE_PTR,
	TYPE_ARRAY,
};

struct type {
	enum type_type type_type;
	union {
		enum keyword keyword;
		struct {
			struct type* child;
			union {
				struct expr* len;
			};
		};
		struct symbol* sym;
	};
	size_t width;
};

size_t type_sizeof(struct type* type);
bool type_cmp(struct type* left, struct type* right);
struct type* builtin(enum keyword key);
struct type* array(struct type* base, struct expr* len);
struct type* ptr(struct type* base);
struct type* struct_or_union(struct symbol* sym);
bool type_check(struct expr* expr);
#endif
