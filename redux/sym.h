#ifndef SYM_H_
#define SYM_H_

#include <stdint.h>
#include <stdbool.h>

#include "vec.h"

struct type;
struct token;
struct symbol;
struct scope;
struct ast;

struct fun_symbol {
	struct type* ret_type;
	vec(struct symbol*) args;
	struct ast* code;
	struct scope* scope;
};

struct enum_map_entry {
	struct enum_map_entry* next;
	size_t key;
	int val;
};

struct struct_map_entry {
	struct struct_map_entry* next;
	size_t key;
	struct type* type;
};

struct union_map_entry {
	struct union_map_entry* next;
	size_t key;
	struct type* type;
};

enum symbol_type {
	SYM_TYPEDEF,
	SYM_UNION,
	SYM_STRUCT,
	SYM_ENUM,
	SYM_FUNCTION,
	SYM_VARIABLE,
};

struct symbol {
	struct symbol* next;
	struct token* defined;
	size_t iden;
	enum symbol_type symbol_type;
	union {
		struct enum_map_entry* enum_map[16];
		struct struct_map_entry* struct_map[16];
		struct union_map_entry* union_map[16];
		struct type* type;
		struct fun_symbol fun;
	};
	int is_local;
	union {
		size_t address;
		size_t offset;
	};
};

struct symbol* variable_declaration(struct scope* scope, struct type* type, struct token* tok);

#endif
