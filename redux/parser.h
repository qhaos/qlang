#ifndef PARSER_H_
#define PARSER_H_

#include "ast.h"
#include "lex.h"
#include "types.h"
#include "vec.h"

struct parser_state {
	struct file* input;
	struct token* curtok;
	struct ast* trees;
	vec(struct scope*) scope_stack;
};

struct ast* parse(struct file* input);

#endif
