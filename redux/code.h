#ifndef CODE_H_
#define CODE_H_

#define START_ADDR 0x00400000

#include <stddef.h>
#include <stdint.h>

struct expr;

enum operation {
	OPER_ADD,
	OPER_SUB,
	OPER_MUL,
	OPER_DIV,
	OPER_SHL,
	OPER_SHR,
	OPER_AND,
	OPER_OR,
	OPER_XOR,
	OPER_PUSH,
	OPER_POP,
	OPER_MOV,
	OPER_DATA,
	OPER_SETZ,
	OPER_SETNZ,
	OPER_NOT,
	OPER_NEG,
	OPER_TEST,
};

enum operand_type {
	OPERAND_REGISTER = (1 << 0),
	OPERAND_IMM = (1 << 1),
	OPERAND_SYMBOL = (1 << 2),
	OPERAND_ADDRESS = (1 << 3),
};

struct operand {
	enum operand_type operand_type;
	union {
		int64_t val;
		int reg;
		struct symbol* sym;
	};
};

struct instruction {
	enum operation op;
	struct operand dst;
	struct operand src;
	int instruction_size;
	size_t address;
	size_t size;
};


struct ast;
void codegen(struct ast*);
extern size_t addr;
extern struct instruction* program;
extern size_t program_len;

#endif
