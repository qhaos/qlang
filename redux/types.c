#include <stdlib.h>

#include "arch.h"
#include "types.h"


struct type* builtin(enum keyword key) {
	struct type* ret = calloc(sizeof(struct type), 1);
	ret->type_type = TYPE_BUILTIN;
	ret->keyword = key;
	ret->width = 1 << ((key+1) >> 1);
	return ret;
}

bool type_cmp(struct type* left, struct type* right) {
	if(left->type_type != right->type_type) {
		return false;
	}

	switch(left->type_type) {
		case TYPE_BUILTIN:
			return (left->keyword == right->keyword);
		break;
		case TYPE_ARRAY:
			size_t leftsize = type_sizeof(left);
			size_t rightsize = type_sizeof(right);
			if(leftsize != rightsize)
				return false;
		case TYPE_PTR:
			return type_cmp(left->child, right->child);
		break;
		case TYPE_ENUM:
		case TYPE_UNION:
		case TYPE_STRUCT:
			abort();
		break;
	}
}

struct type* array(struct type* base, struct expr* len) {
	struct type* ret = calloc(sizeof(struct type), 1);

	ret->type_type = TYPE_ARRAY;
	ret->child = base;
	ret->len = len;
	/* must be calculated later */
	ret->width = -1;

	return ret;
}

struct type* ptr(struct type* base) {
	struct type* ret = calloc(sizeof(struct type), 1);

	ret->type_type = TYPE_PTR;
	ret->child = base;
	ret->width = PTR_SIZE;

	return ret;
}
