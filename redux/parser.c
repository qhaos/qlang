#include <assert.h>

#include "iden.h"
#include "lex.h"
#include "parser.h"
#include "print.h"
#include "scope.h"
#include "sym.h"
#include "vec.h"

static struct type* parse_type(struct parser_state* state);
static struct expr* parse_expr(struct parser_state*);

static struct expr* binop(struct token* tok, struct expr* left, int op, struct expr* right) {
	struct expr* ret = calloc(sizeof(struct expr), 1);

	ret->expr_type = EXPR_BINOP;
	ret->tok = tok;
	ret->binop.left = left;
	ret->binop.op = op;
	ret->binop.right = right;

	return ret;
}

static struct expr* unop(struct token* tok, int op, struct expr* child) {
	struct expr* ret = calloc(sizeof(struct expr), 1);

	ret->expr_type = EXPR_UNOP;
	ret->tok = tok;
	ret->unop.child = child;
	ret->unop.op = op;

	return ret;
}


static struct token* next_tok(struct parser_state* state) {
	if(!state->curtok->next) {
		print_error(state->curtok, "Unexpected EOF");
	}

	return (state->curtok = state->curtok->next);
}

static struct expr* num(int64_t x, struct token* tok) {
	struct expr* ret = calloc(sizeof(struct expr), 1);

	ret->expr_type = EXPR_NUM;
	ret->num = x;
	ret->tok = tok;

	return ret;
}

static struct expr* var(struct token* tok, struct symbol* sym) {
	struct expr* ret = calloc(sizeof(struct expr), 1);

	*ret = (struct expr) {
		.expr_type = EXPR_VAR,
		.tok = tok,
		.type = sym->type,
		.sym = sym
	};

	return ret;
}

static struct expr* cast(struct token* tok, struct type* type, struct expr* rhs) {
	struct expr* ret = calloc(sizeof(struct expr), 1);

	ret->tok = tok;
	ret->expr_type = EXPR_CAST;
	ret->type = type;
	ret->child = rhs;

	return ret;
}

static struct expr* idx(struct expr* base, struct expr* offset) {
	struct expr* ret = calloc(sizeof(struct expr), 1);
	if(base->type->type_type != TYPE_ARRAY && base->type->type_type != TYPE_PTR) {
		print_error(base->tok, "base of index expression must be of pointer or array type!");
		return NULL;
	}
	ret->expr_type = EXPR_IDX;
	ret->idx.child = base;
	ret->idx.idx = offset;
	ret->type = base->type->child;
	return ret;
}

static int is_unary(enum op op) {
	switch(op) {
		case OP_MINUS:
		case OP_ADD:
		case OP_BNOT:
		case OP_LNOT:
		case OP_BAND:
			return 1;
		default:
			return 0;
		break;
	}
}

static struct expr* parse_primary(struct parser_state* state) {
	struct expr* ret = NULL;
	struct token* tok;
	switch(state->curtok->type) {
		case TOK_KEYWORD:
			print_error(state->curtok, "unexpected keyword");
			next_tok(state);
		break;
		case '{':
		case '}':
		case ')':
		case '[':
		case ']':
		case ',':
		case ';':
			print_error(state->curtok, "unexpected '%c'", state->curtok->type);
			next_tok(state);
		break;
		case '(':
		struct token* save = state->curtok;
		next_tok(state);
		struct type* type = parse_type(state);
		if(!type) {
			/* not a cast, but a parenthetical expression */
			ret = parse_expr(state);
		}
		/* in either case, check for a closing ')' */
		if(state->curtok->type != ')') {
			print_error(state->curtok, "expected ')' to match '(' from %d:%d", save->row, save->col);
		} else {
			next_tok(state);
		}
		if(type)
			ret = cast(save, type, parse_expr(state));
		break;
		case TOK_OP:
			if(is_unary(state->curtok->op)) {
				struct token* save = state->curtok;
				next_tok(state);
				struct expr* child = parse_primary(state);
				ret = unop(save, save->op, child);
				return ret;
			} else {
				print_error(state->curtok, "unexpected binary operator");
			}
		break;
		case TOK_IDEN:
			tok = state->curtok;
			struct symbol* sym = scope_find(*vec_back(state->scope_stack), tok->iden);
			if(!sym) {
				print_error(tok, "identifier undefined in this scope");
			}
			next_tok(state);
			if(state->curtok->type == '(') {
				/* function call */
				if(sym->symbol_type != SYM_FUNCTION) {
					print_error(tok, "you cannot call something other than a function (for now)");
				}
				next_tok(state);
				ret = calloc(sizeof(struct expr), 1);
				*ret = (struct expr) {
					.expr_type = EXPR_FCALL,
					.tok = tok,
					.type = sym->fun.ret_type,
					.fcall = {
						.sym = sym,
						.args = vec_new(struct expr*, vec_len(sym->fun.args))
					}
				};
				bool first = true;
				while(state->curtok->type != ')') {
					if(!first) {
						if(state->curtok->type != ',') {
							print_error(state->curtok, "expected ',' or ')'");
						}
						next_tok(state);
					}
					first = false;
					vec_push(ret->fcall.args, parse_expr(state));
				}
				next_tok(state);
			} else {
				ret = var(tok, sym);
			}
		break;
		case TOK_NUM:
			struct token* tok = state->curtok;
			next_tok(state);
			return num(tok->num, tok);
		break;
	}

	if(state->curtok->type == '[') {
		next_tok(state);
		struct expr* i = parse_expr(state);
		if(state->curtok->type != ']') {
			print_error(state->curtok, "expected ']' to match '['");
		}
		return idx(ret, i);
	}

	return ret;
}

/* operator precedence chart, stolen directly from C */
static int prec[] = {
	[OP_EQ] = 7,
	[OP_ASSGN] = 1,
	[OP_NOTEQ] = 7,
	[OP_LNOT] = 0,
	[OP_ADDASSGN] = 1,
	[OP_ADD] = 9,
	[OP_SUBASSGN] = 1,
	[OP_MINUS] = 9,
	[OP_MULASSGN] = 1,
	[OP_STAR] = 10,
	[OP_DIVASSGN] = 1,
	[OP_DIV] = 10,
	[OP_MODASSGN] = 1,
	[OP_MOD] = 10,
	[OP_LSHIFTASSGN] = 1,
	[OP_LSHIFT] = 9,
	[OP_RSHIFTASSGN] = 1,
	[OP_RSHIFT] = 9,
	[OP_LESSEQ]= 8,
	[OP_MOREEQ]= 8,
	[OP_LESS]= 8,
	[OP_MORE] = 8,
	[OP_BNOT] = 0,
	[OP_LAND] = 3,
	[OP_BANDASSGN] = 1,
	[OP_BAND] = 6,
	[OP_LOR] = 2,
	[OP_BORASSGN] = 1,
	[OP_BOR] = 4,
	[OP_XORASSGN] = 1,
	[OP_XOR] = 5,	
};

static struct expr* parse_rhs(struct parser_state* state, struct expr* lhs, int minprec) {
	while(1) {
		if(state->curtok->type != TOK_OP)
			return lhs;
		/* screen for unary-only */
		switch(state->curtok->op) {
			case OP_LNOT:
			case OP_BNOT:
				return lhs;
			break;
			default:
			break;
		}

		int curprec = prec[state->curtok->op];

		if(curprec < minprec) {
			return lhs;
		}
		/* save token for later when I construct the binop() */
		struct token* save = state->curtok;

		next_tok(state);

		struct expr* rhs = parse_primary(state);

		/* check for a higher precedence operator, and if so, switch it out for this one */
		if(state->curtok->type == TOK_OP && curprec < prec[state->curtok->op]) {
			rhs = parse_rhs(state, rhs, curprec + 1);
			if(!rhs)
				return rhs;
		}

		lhs = binop(save, lhs, save->op, rhs);
		
	}
}

static struct expr* parse_expr(struct parser_state* state) {
	struct expr* lhs = parse_primary(state);
	if(!lhs)
		return NULL;
	return parse_rhs(state, lhs, 0);
}

static struct ast* stmt(enum stmt_type type, void* val) {
	struct ast* ret = calloc(sizeof(struct ast), 1);
	ret->ast_type = AST_STMT;
	ret->stmt_type = type;
	switch(type) {
		case STMT_IF:
			ret->if_stmt = *(struct if_stmt*) val;
		break;
		case STMT_EXPR:
			ret->expr = (struct expr*) val;
		break;
		case STMT_VARDEC:
			ret->vardec.sym = (struct symbol*) val;
		break;
		case STMT_BLOCK:
			ret->block_stmt = *(struct block*) val;
		break;
	}

	return ret;
}

static struct expr* fcall(struct token* tok, struct symbol* sym, vec(struct expr*) args) {
	struct expr* ret = calloc(sizeof(struct expr), 1);

	ret->expr_type = EXPR_FCALL;
	ret->tok = tok;
	ret->fcall.sym = sym;
	ret->fcall.args = args;

	return ret;	
}

/*
	type_primary :- keyword | id
	type :- type_primary | type'*' | type '[' expr ']'
*/
static struct type* parse_type_primary(struct parser_state* state) {
	struct token* tok = state->curtok;
	struct type* ret = NULL;
	switch(tok->type) {
		case TOK_KEYWORD:
			switch(tok->keyword) {
				case KEY_VOID:
				case KEY_U8:
				case KEY_U16:
				case KEY_U32:
				case KEY_U64:
				case KEY_I8:
				case KEY_I16:
				case KEY_I32:
				case KEY_I64:
					ret = calloc(sizeof(struct type), 1);
					ret->type_type = TYPE_BUILTIN;
					ret->keyword = tok->keyword;
					ret->width = -1;
					next_tok(state);
					return ret;
				break;
				default:
					return NULL;
				break;
			}
		break;
		case TOK_IDEN:
			struct symbol* sym = scope_find(*vec_back(state->scope_stack), tok->iden);

			if(!sym)
				return NULL;

			switch(sym->symbol_type) {
				case SYM_ENUM:
				case SYM_UNION:
				case SYM_STRUCT:
					ret = calloc(sizeof(struct type), 1);
					ret->type_type = (enum type_type)(sym->symbol_type);
					ret->sym = sym;
					next_tok(state);
				break;
				case SYM_TYPEDEF:
					next_tok(state);
					return sym->type;
				break;
				default:
					return NULL;
				break;
			}
		break;
		default:
		break;
	}

	return ret;
}

/* arrays, ptr-ness, etc */
static struct type* parse_type_extra(struct parser_state* state, struct type* base) {
	struct type* ret = NULL;
	switch(state->curtok->type) {
		case '[':
			next_tok(state);
			if(state->curtok->type == ']') {
				next_tok(state);
				return array(base, num(0, state->curtok));
			}
			ret = array(base, parse_expr(state));
			if(state->curtok->type != ']') {
				print_error(state->curtok, "expected closing ']' in array type");
				return NULL;
			}
			next_tok(state);
			return ret;
		break;
		case TOK_OP:
			if(state->curtok->op == OP_STAR) {
				next_tok(state);
				return ptr(base);
			}
		break;
		default:
		break;
	}

	return base;
}

static struct type* parse_type(struct parser_state* state) {
	/* save in order to backtrack */
	struct token* save = state->curtok;

	struct type* ret = parse_type_primary(state);

	if(!ret) {
		state->curtok = save;
		return NULL;
	}

	struct type* last;
	do {
		last = ret;
		ret = parse_type_extra(state, ret);
	} while(last != ret);
	
	if(!ret) {
		state->curtok = save;
		return NULL;
	}

	return ret;
}

static struct symbol* parse_variable_declaration(struct parser_state* state, struct scope* scope) {
	struct type* type = parse_type(state);
	if(state->curtok->type != TOK_IDEN) {
		free(type);
		print_error(state->curtok, "expected identifier after typename");
		return NULL;
	}

	struct token* save = state->curtok;
	struct symbol* ret = variable_declaration(scope, type, state->curtok);
	if(!ret) {
		print_error(save, "redeclaration of previously declared symbol");
	}
	next_tok(state);

	return ret;
}

static struct ast* parse_block_contents(struct parser_state* state);

static struct ast* parse_if_statement(struct parser_state* state) {
	struct ast* ret = calloc(sizeof(struct ast), 1);
	ret->ast_type = AST_STMT;
	ret->stmt_type = STMT_IF;
	next_tok(state);

	ret->if_stmt.condition = parse_expr(state);
	if(state->curtok->type != '{')
		print_error(state->curtok, "expected block after if statement");
	ret->if_stmt.stmt = parse_block_contents(state);

	if(state->curtok->type == TOK_KEYWORD && state->curtok->keyword == KEY_ELSE) {
		next_tok(state);
		if(state->curtok->type == TOK_KEYWORD && state->curtok->keyword == KEY_IF) {
			ret->if_stmt.else_chain = parse_if_statement(state);
		} else {
			ret->if_stmt.else_chain = calloc(sizeof(struct ast), 1);
			ret->if_stmt.else_chain->ast_type = AST_STMT;
			ret->if_stmt.else_chain->stmt_type = STMT_IF;

			ret->if_stmt.else_chain->if_stmt.condition = NULL;
			if(state->curtok->type != '{')
				print_error(state->curtok, "expected block after if statement");
			ret->if_stmt.else_chain->if_stmt.stmt = parse_block_contents(state);
		}
	}

	return ret;
}

static struct ast* parse_block_contents(struct parser_state* state) {
	struct ast* ret = NULL;
	switch(state->curtok->type) {
		case '}':
			state->curtok = state->curtok->next;
			return NULL;
		break;
		case ';':
			next_tok(state);
			return parse_block_contents(state);
		break;
		case '{':
			next_tok(state);
			ret = calloc(sizeof(struct ast), 1);
			ret->ast_type = AST_STMT;
			ret->stmt_type = STMT_BLOCK;
			ret->block_stmt.scope = scope(*vec_back(state->scope_stack));
			vec_push(state->scope_stack, ret->block_stmt.scope);
			ret->block_stmt.code = parse_block_contents(state);
			vec_pop(state->scope_stack);
			return ret;
		break;
		case TOK_KEYWORD: {
			switch(state->curtok->keyword) {
				case KEY_VOID:
				case KEY_U8:
				case KEY_U16:
				case KEY_U32:
				case KEY_U64:
				case KEY_I8:
				case KEY_I16:
				case KEY_I32:
				case KEY_I64:
				break;
				case KEY_IF:
					ret = parse_if_statement(state);
					goto end;
				break;
				default:
					assert("man I'm great at this whole thing" && 0);
				break;
			}
		}
		case TOK_IDEN: {
			struct token* save = state->curtok;
			struct type* type = parse_type(state);
			free(type);
			if(!type) {
				ret = stmt(STMT_EXPR, parse_expr(state));
				break;
			}
			state->curtok = save;
			struct symbol* sym = parse_variable_declaration(state, *vec_back(state->scope_stack));
			if(sym) {
				ret = stmt(STMT_VARDEC, sym);
				if(!ret)
					abort();
				if(state->curtok->type == ';') {
					next_tok(state);
					goto end;
				}
				if(state->curtok->type == TOK_OP && state->curtok->op == OP_ASSGN) {
					next_tok(state);
					ret->vardec.initializer = parse_expr(state);
				} else {
					print_error(state->curtok, "expected '=' or ';'");
				}
				goto end;
			} else {
				print_error(save, "redefinition of %s within same scope", identbl[save->iden].data);
				next_tok(state);
			}
		}
		case '(':
		case TOK_NUM:
			ret = stmt(STMT_EXPR, parse_expr(state));
		break;
		default:
			print_error(state->curtok, "expected identifier, number, '*', ';', ')', or '}'");
		break;
	}

	end:
	ret->next = parse_block_contents(state);
	return ret;
}

static struct ast* parse_function_declaration(struct parser_state* state, struct type* ret_type, struct token* defined) {
	struct ast* ret = calloc(sizeof(struct ast), 1);
	size_t iden = defined->iden;
	ret->ast_type = AST_FUNDECL;

	struct symbol* sym = calloc(sizeof(struct symbol), 1);
	*sym = (struct symbol){
		.symbol_type = SYM_FUNCTION,
		.defined = defined,
		.iden = iden,
		.fun = {.ret_type = ret_type}
	};
	
	vec(struct symbol*) args = vec_new(struct symbol*, 0);

	bool first = true;
	while(state->curtok->type != ')') {
		if(!first) {
			if(state->curtok->type != ',') {
				print_error(state->curtok, "expected ',' or ')'");
			}
			next_tok(state);
		}
		first = false;
		vec_push(args, parse_variable_declaration(state, NULL));
	}

	sym->fun.args = args;

	/* if changed is set then there is already a degined function complete with code */
	bool changed = false;
	struct symbol* ret_sym = scope_add(&global, sym, &changed);

	ret->fundecl.sym = ret_sym;

	/* curtok is currently at ')' */
	next_tok(state);
	
	if(state->curtok->type == ';') {
		state->curtok = state->curtok->next;
		return ret;
	}

	if(state->curtok->type == '{') {
		if(changed) {
			print_error(state->curtok, "function already has a body from a previous declaration");
			return NULL;
		}
		if(sym != ret_sym) {
			vec_destroy(ret_sym->fun.args);
			ret_sym->fun.args = sym->fun.args;
			free(sym);
			sym = ret_sym;
		}
		next_tok(state);
		struct scope* new = scope(&global);
		vec_push(state->scope_stack, new);
		for(size_t i = 0; i < vec_len(args); i++) {
			scope_add(new, args[i], NULL);
		}
		sym->fun.scope = new;
		sym->fun.code = parse_block_contents(state);
		ret->fundecl.is_complete = 1;
		vec_pop(state->scope_stack);
	}

	return ret;
}

static struct ast* parse_toplevel(struct parser_state* state) {
	if(state->curtok == NULL)
		return NULL;

	struct ast* ret = NULL;

	switch(state->curtok->type) {
		case TOK_NUM:
			print_error(state->curtok, "unexpected number");
			next_tok(state);
		break;
		case TOK_KEYWORD:
			switch(state->curtok->keyword) {
				case KEY_VOID:
				case KEY_U8:
				case KEY_U16:
				case KEY_U32:
				case KEY_U64:
				case KEY_I8:
				case KEY_I16:
				case KEY_I32:
				case KEY_I64:
				break;
				default:
					goto _default;
				break;
			}
		case TOK_IDEN:
			struct type* type = parse_type(state);

			if(type == NULL) {
				print_error(state->curtok, "expected typename");
				break;
			} else if(state->curtok->type != TOK_IDEN) {
				print_error(state->curtok, "expected identifier after typename");
				break;
			}

			struct token* defined = state->curtok;

			next_tok(state);

			if(state->curtok->type == '(') {
				next_tok(state);
				ret = parse_function_declaration(state, type, defined);
				vec_pop(state->scope_stack);
				goto end;
			}

			struct symbol* sym = variable_declaration(&global, type, defined);

			if(!sym) {
				print_error(state->curtok, "redeclaration of symbol %s", identbl[defined->iden].data);
				ret = NULL;
				goto end;
			}

			ret = stmt(STMT_VARDEC, sym);
			if(state->curtok->type == ';') {
				next_tok(state);
				goto end;
			}
			if(state->curtok->type == TOK_OP && state->curtok->op == OP_ASSGN) {
				next_tok(state);
				ret->vardec.initializer = parse_expr(state);
			} else {
				print_error(state->curtok, "expected '=' or ';'");
			}
			goto end;
		break;
		default:
			_default:
			print_error(state->curtok, "unexpected token at top level");
			state->curtok = state->curtok->next;
		break;
	}

	end:
 	if(ret) {
 		ret->next = parse_toplevel(state);
 		return ret;
 	} else {
 		return parse_toplevel(state);
 	}
}

struct ast* parse(struct file* input) {
	struct parser_state* state = calloc(sizeof(struct parser_state), 1);

	*state = (struct parser_state) {
		.input = input,
		.curtok = input->tokens,
		.trees = NULL,
		.scope_stack = vec_new(struct scope*, 1)
	};

	vec_push(state->scope_stack, &global);
	
	state->trees = parse_toplevel(state);

	return state->trees;
}
