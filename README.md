# Qlang - A programming experience

You have traveled long and far, through mountain high and valley low.  
The pursuit of knowledge eternal is what fuels your soul through this perilous and taxing 
journey.

Well, look no further!

Presenting Qh4lang, ~~complete~~ with:
1. Working hello world! (check /examples)
2. FASM backend
3. Exceptionally rudiementary type inferencing (basically never worth it)
4. Functions!
5. Some working math!
6. Pointers!

## Compiling
```sh
cc src/*.c -o qcc
```
