#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>

#include "code.h"
#include "hacks.h"
#include "opt.h"
#include "parse.h"
#include "print.h"
#include "src.h"
#include "tok.h"

int main(int argc, char* argv[]) {
	FILE* in = stdin;

	if(argc > 1) {
		in = fopen(argv[1], "r");
		if(!in) {
			perror("fopen");
			return 1;
		}
	}

	readfile(in);

	int e = lex();

	if(e) {
		fprintf(stderr, "Compilation terminated (%d lexical errors)\n", e);
		for(size_t i = 0; i < n_tokens; i++) {
			if(tokens[i].type == TOK_ERR) {
				print_tok(&tokens[i]);
			}
		}

		return 1;
	}

	struct ast* root = parse();

	gen(root);

	return 0;
}
