#ifndef PRINT_H_
#define PRINT_H_

#include "ast.h"
#include "tok.h"

void print_tok(struct tok* tok);
void print_error(struct tok* tok, const char* err, ...);
void print_expr(struct expr_ast* root);
void print_ast(struct ast* root);
void print_sym(struct symbol* sym);
void print_scope(struct scope* scope);

#endif
