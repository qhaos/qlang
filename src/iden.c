#include <stdlib.h>
#include <string.h>

#include "hacks.h"
#include "iden.h"

struct iden* identbl = NULL;

static size_t identbl_len = 0;


size_t register_iden(const char* data, size_t len, uint64_t hash) {
	size_t i;
	for(i = 0; i < identbl_len; i++) {
		if(hash == identbl[i].hash &&
		   identbl[i].len == len &&
		   !strncmp(data, identbl[i].data, len)) {
				return i;
		}
	}

	identbl_len++;
	identbl = realloc(identbl, sizeof(struct iden) * identbl_len);
	identbl[i].hash = hash;
	identbl[i].len = len;
	identbl[i].data = strndup(data, len);

	return i;
}
