#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#include "ast.h"
#include "iden.h"
#include "print.h"
#include "string.h"
#include "src.h"

static void print_str(const char* str) {
	putc('"', stderr);
	size_t len = strlen(str);
	for(size_t i = 0; i < len; i++) {
		if(isprint(str[i])) {
			if(str[i] == '\\') {
				fputs("\\\\", stderr);
			} else if(str[i] == '"') {
				fputs("\\\"", stderr);
			} else {
				putc(str[i], stderr);
			}
		} else if(isspace(str[i])) {
			switch(str[i]) {
				case '\t':
					fputs("\\t", stderr);
				break;
				case '\v':
					fputs("\\v", stderr);
				break;
				case '\n':
					fputs("\\n", stderr);
				break;
				case '\f':
					fputs("\\n", stderr);
				break;
				case '\r':
					fputs("\\r", stderr);
				break;
			}
		} else {
			fprintf(stderr, "\\%2x", str[i]);
		}
	}
	putc('"', stderr);
	
}

void print_tok(struct tok* tok) {
	int color = 33;
	fprintf(stderr, "[%d:%d] ", tok->row+1, tok->col+1);
	switch(tok->type) {
		case TOK_KEYWORD:
			fprintf(stderr, "keyword: %s\n", keywords[tok->key]);
		break;
		case TOK_NUM:
			fprintf(stderr, "number: %ld (0x%lX):\n", tok->num, tok->num);
		break;
		case TOK_IDEN: {
			struct iden* id = &identbl[tok->iden];
			fprintf(stderr, "iden: { .hash = 0x%lX, .data = \"%s\" (%p), .len = %zu }:\n",
							id->hash, id->data, id->data, id->len);
		}
		break;
		case TOK_OP: {
			fprintf(stderr, "operator: %s (#%d):\n", ops[tok->op], tok->op);
		}
		break;
		case TOK_ERR:
			color = 31;
			fprintf(stderr, "\033[31merror:\033[m %s:\n", lex_errlist[tok->err]);
		break;
		case TOK_STRING: {
			fprintf(stderr, "string: (#%zu, len: %zu) ", tok->string, stringtbl[tok->string].len);
			print_str(stringtbl[tok->string].data);
			putc('\n', stderr);
		}
		break;
		default: {
			fprintf(stderr, "%c (0x%X):\n", tok->type, tok->type);
		}
		break;
	}

	fprintf(stderr, "\t%4d | ", tok->row+1);
	fwrite(lines[tok->row], 1, tok->col, stderr);
	fprintf(stderr, "\033[%dm", color);
	fwrite(lines[tok->row]+tok->col, 1, tok->len, stderr);
	fprintf(stderr, "\033[m%s", lines[tok->row]+tok->col+tok->len);

	fprintf(stderr, "\t     | ");
	for(int i = 0; i < tok->col; i++) {
		if(lines[tok->row][i] == '\t') {
			putc('\t', stderr);
		} else {
			putc(' ', stderr);
		}
	}
	fprintf(stderr, "\033[%dm^", color);
	for(size_t i = 1; i < tok->len; i++)
		putc('~', stderr);
	fputs("\033[m\n", stderr);
}

void print_error(struct tok* tok, const char* err, ...) {
	va_list v;
	va_start(v, err);
	fprintf(stderr, "[%d:%d] \033[31merror:\033[m ", tok->row+1, tok->col+1);
	vfprintf(stderr, err, v);
	fprintf(stderr, ":\n\t%4d | ", tok->row+1);
	fwrite(lines[tok->row], 1, tok->col, stderr);
	fprintf(stderr, "\033[31m");
	fwrite(lines[tok->row]+tok->col, 1, tok->len, stderr);
	fprintf(stderr, "\033[m%s", lines[tok->row]+tok->col+tok->len);

	fprintf(stderr, "\t     | ");
	for(int i = 0; i < tok->col; i++) {
		if(lines[tok->row][i] == '\t') {
			putc('\t', stderr);
		} else {
			putc(' ', stderr);
		}
	}
	fprintf(stderr, "\033[31m^");
	for(size_t i = 1; i < tok->len; i++)
		putc('~', stderr);
	fputs("\033[m\n", stderr);

	va_end(v);
	/* for now just instantly bail on any error */
	exit(1);
}


void print_type(struct type*);

void print_expr(struct expr_ast* root) {
	if(!root)
		return;
	putc('(', stderr);
	print_type(&root->type);
	putc(')', stderr);
	switch(root->exprtype) {
		case EXPR_VAR:
			fprintf(stderr, "%s", identbl[root->sym->iden].data);
		break;
		case EXPR_STRING:
			print_str(stringtbl[root->string].data);
		break;
		case EXPR_NUM:
			fprintf(stderr, "%ld", root->num);
		break;
		case EXPR_UNOP:
			putc('(', stderr);
				fprintf(stderr, "%s", ops[root->op]);
				print_expr(root->child);
			putc(')', stderr);
		break;
		case EXPR_BINOP:
			putc('(', stderr);
				print_expr(root->binop.left);
				fprintf(stderr, " %s ", ops[root->op]);
				print_expr(root->binop.right);
			putc(')', stderr);
		break;
		case EXPR_DEREF:
			putc('[', stderr);
				print_expr(root->child);
			putc(']', stderr);
		break;
		case EXPR_ARRAY:
			fputs("{ ", stderr);
			for(size_t i = 0; i < root->type.len; i++) {
				if(i != 0)
					fputs(", ", stderr);
				print_expr(root->array[i]);
			}
			fputs(" }", stderr);
		break;
		case EXPR_SYSCALL:
			fputs("syscall(", stderr);
				for(size_t i = 0; i < root->syscall.n_args; i++) {
					print_expr(root->syscall.args[i]);
					if(i != root->syscall.n_args - 1)
						putc(',', stderr);
				}
			putc(')', stderr);
		break;
	}
}

void print_type(struct type* type) {
	if(type->flags & TYPE_CONST)
		fputs("const ", stderr);

	if(type->flags & TYPE_BUILTIN) {
		fputs(keywords[type->key], stderr);
	} else {
		fputs(identbl[type->sym->iden].data, stderr);
	}

	for(int i = 0; i < type->ptr; i++)
		putc('*', stderr);
	if(type->flags & TYPE_ARRAY) {
		fprintf(stderr, "[%zu]", type->len);
	}
}

void _print_ast(struct ast* head, int lvl) {
	if(!head)
		return;

	for(int i = 0; i < lvl; i++) {
		putc('\t', stderr);
	}

	switch(head->type) {
		case AST_STMT:
			print_expr(head->expr);
			putc(';', stderr);
		break;
		case AST_VARDEF:
			print_type(&head->vardef->type);
			putc(' ', stderr);
			fputs(identbl[head->vardef->iden].data, stderr);
			if(head->vardef->initializer) {
				fputs(" = ", stderr);
				print_expr(head->vardef->initializer);
			}
			putc(';', stderr);
		break;
		case AST_IF:
			fputs("if ", stderr);
			print_expr(head->if_construct.cond);
			fputs(" {\n", stderr);
				_print_ast(head->if_construct.block.code, lvl+1);
				for(int i = 0; i < lvl; i++) {
					putc('\t', stderr);
				}
				putc('}', stderr);
		break;
		case AST_BLOCK:
			fputs("{\n", stderr);
				_print_ast(head->block.code, lvl+1);
			putc('}', stderr);
		break;
		case AST_FUN:
			print_type(&head->fun.ret_type);
			fprintf(stderr, " %s(", identbl[head->fun.sym->iden].data);
			for(size_t i = 0; i < head->fun.n_args; i++) {
				print_type(&head->fun.arguements[i]->type);
				printf(" %s", identbl[head->fun.arguements[i]->iden].data);
				if(i != head->fun.n_args-1)
					fputs(", ", stderr);
			}
			putc(')', stderr);

			if(head->fun.is_defined) {
				fputs(" {\n", stderr);
					_print_ast(head->fun.code, lvl+1);
				putc('}', stderr);
			}
		break;
	}

	putc('\n', stderr);
	_print_ast(head->next, lvl);
}

void print_ast(struct ast* head) {
	_print_ast(head, 0);
}

void print_sym(struct symbol* sym) {
	fprintf(stderr, "defined at: %d:%d\n", sym->defined->row, sym->defined->col);
	print_tok(sym->defined);
	fprintf(stderr, "\"%s\"(%zu)\n", identbl[sym->iden].data, sym->iden);
}

void print_scope(struct scope* scope) {
	if(!scope->usage) {
		fprintf(stderr, "empty scope\n");
		return;
	}
	fprintf(stderr, "scope contents:\n");
	for(size_t i = 0; i < 16; i++) {
		if(scope->usage & (1 << i)) {
			for(struct symbol* t = &scope->symbols[i]; t; t = t->next) {
				putc('\t', stderr);
				print_sym(t);
			}
		}
	}
}
