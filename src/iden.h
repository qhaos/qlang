#ifndef IDEN_H_
#define IDEN_H_

#include <stddef.h>
#include <stdint.h>

struct iden {
	char* data;
	size_t len;

	uint64_t hash;
};

extern struct iden* identbl;

size_t register_iden(const char* data, size_t len, uint64_t hash);

#endif
