#include <string.h>

#include "hacks.h"
#include "iden.h"
#include "print.h"
#include "scope.h"

struct scope global = {0};

struct symbol* find_sym(struct scope* scope, size_t iden, int type) {
	size_t hash = iden & 0xF;
	if((1 << hash) & scope->usage) {
		for(struct symbol* sym = &scope->symbols[hash]; sym; sym = sym->next) {
			if(iden == sym->iden && type & sym->sym_type)
				return sym;
		}
	}

	if(!scope->parent)
		return NULL;
	else
		return find_sym(scope->parent, iden, type);
}

struct symbol* add_sym(struct scope* scope, struct symbol* sym) {
	size_t hash = sym->iden & 0xF;

	sym->global = scope == &global;
	
	if((1 << hash) & scope->usage) {
		for(struct symbol* i = &scope->symbols[hash]; i; i = i->next) {
			if(i->iden == sym->iden) {
				print_error(
					sym->defined,
					"redeclaration of symbol \"%s\" in same scope (previously declared %d:%d)",
					identbl[i->iden].data,
					i->defined->row,
					i->defined->col
				);
				return NULL;
			}
			if(!i->next) {
				i->next = calloc(sizeof(struct symbol), 1);
				*i->next = *sym;
				return i->next;
			}
		}
	}

	/* "ez-pz" */
	scope->usage |= (1 << hash);
	scope->symbols[hash] = *sym;
	return &scope->symbols[hash];
}
