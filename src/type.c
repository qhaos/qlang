#include <string.h>

#include "type.h"
#include "sym.h"
#include "ast.h"

int is_implicitly_convertable(struct type* lhs, struct type* rhs) {
	if(memcmp(lhs, rhs, sizeof(struct type)) == 0)
		return 1;

	int adj_ptrlvll = lhs->ptr + (!!(lhs->flags & TYPE_ARRAY));
	int adj_ptrlvlr = rhs->ptr + (!!(rhs->flags & TYPE_ARRAY));
	if (adj_ptrlvll != adj_ptrlvlr) {
		return 0;
	}

	if(((~lhs->flags) & (~rhs->flags) & TYPE_BUILTIN) && rhs->sym != lhs->sym)
		return 0;

	if((lhs->flags & TYPE_ARRAY) && !(rhs->flags & TYPE_ARRAY)) {
		return 0;
	}
	
	if(rhs->flags & TYPE_ARRAY) {
		if(lhs->key != rhs->key) {
			return 0;	
		} else if(lhs->flags & TYPE_ARRAY) {
			if(lhs->len == 0) {
				lhs->len = rhs->len;
			} else if(lhs->len != rhs->len) {
				return 0;
			}
		}
	}

	return 1;
}

void implicit_cast(struct type* lhs, struct expr_ast* rhs) {
	/* assume it's already been checked for validity */
	if(memcmp(lhs, &rhs->type, sizeof(struct type)) == 0)
		return;

	if(rhs->type.flags & TYPE_ARRAY) {
		rhs->type.flags &= ~TYPE_ARRAY;
		rhs->type.ptr++;
		rhs->type.len = 0;
		rhs->type.cast_me = 1;
	} else {
		rhs->type.key = lhs->key;
	}
}
