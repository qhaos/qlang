#ifndef SCOPE_H_
#define SCOPE_H_

#include "sym.h"

struct scope {
	struct scope* parent;
	/* super symbol map */
	uint16_t usage;
	struct symbol symbols[16];
};

extern struct scope global;

struct symbol* find_sym(struct scope* scope, size_t iden, int type);
struct symbol* add_sym(struct scope* scope, struct symbol* sym);

#endif
