#ifndef STRING_H_
#define STRING_H_

#include <stddef.h>

struct string {
	char* data;
	size_t len;
};

extern struct string* stringtbl;
extern size_t stringtbl_len;

#endif
