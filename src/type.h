#ifndef TYPE_H_
#define TYPE_H_

enum typeflags {
	TYPE_BUILTIN	= (1 << 0),
	TYPE_CONST		= (1 << 1),
	TYPE_ARRAY		= (1 << 2),
};

struct symbol;

struct type {
	int flags;
	int ptr;
	union {
		int key;
		struct symbol* sym;
	};
	int cast_me;
	size_t len;
};


struct expr_ast;

int is_implicitly_convertable(struct type* lhs, struct type* rhs);
void implicit_cast(struct type* lhs, struct expr_ast* rhs);

#endif
