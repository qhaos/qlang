#ifndef TOK_H_
#define TOK_H_

#include <stddef.h>
#include <stdint.h>

enum toktype {
	TOK_ERR,
	TOK_IDEN,
	TOK_OP,
	TOK_KEYWORD,
	TOK_NUM,
	TOK_STRING,
};

enum keyword {
	KEY_U8,
	KEY_I8,
	KEY_U16,
	KEY_I16,
	KEY_U32,
	KEY_I32,
	KEY_U64,
	KEY_I64,
	KEY_IF,
	KEY_FN,
	KEY_CONST,
	KEY_SIZEOF,
	KEY_LET,
	KEY_RETURN,
	KEY_SYSCALL,
};

static const char* keywords[] = {
	"u8",
	"i8",
	"u16",
	"i16",
	"u32",
	"i32",
	"u64",
	"i64",
	"if",
	"fn",
	"const",
	"sizeof",
	"let",
	"return",
	"syscall",
};

enum op {
	OP_ADD,
	OP_SUB,
	OP_STAR,
	OP_DIV,
	OP_MOD,
	OP_LESSEQ,
	OP_MOREEQ,
	OP_LSHFT,
	OP_RSHFT,
	OP_LESS,
	OP_MORE,
	OP_EQ,
	OP_NEQ,
	OP_ASSGN,
	OP_AND,
	OP_OR,
	OP_XOR,
	OP_NOT,
	OP_LNOT,
	OP_ADDR,
};

static const int prec[] = {
	9,
	9,
	10,
	10,
	10,
	7,
	7,
	8,
	8,
	7,
	7,
	7,
	6,
	6,
	1,
	5,
	3,
	4,
	11,
	11,
	11,
};

static const char* ops[] = {
	"+",
	"-",
	"*",
	"/",
	"%",
	"<=",
	">=",
	"<<",
	">>",
	"<",
	">",
	"==",
	"!=",
	"=",
	"&",
	"|",
	"^",
	"~",
	"!",
	"@",
};

struct tok {
	int row;
	int col;
	int type;
	size_t len;
	union {
		int op;
		size_t iden;
		size_t key;
		int64_t num;
		size_t string;
		int err;
	};
};

static const char *lex_errlist[] = {
	"no error, the compiler just sucks.",
	"unrecognized character",
	"invalid number",
	"invalid escape sequence",
	"unterminated string",
};

enum errors {
	ERR_OK,
	ERR_UNRECOGNIZED,
	ERR_INVNUM,
	ERR_INVESCAPE,
	ERR_UNTERMINATEDSTRING,
};

extern struct tok* tokens;
extern size_t n_tokens;

/* returns number of errors */
int lex();

#endif
