#include <stddef.h>

#include "hacks.h"
#include "sizeof.h"

size_t expr_sizeof(struct type* type) {
	size_t prelim = (1 << (type->key >> 1));

	if(type->ptr) {
		prelim = 8;
	}

	if(type->flags & TYPE_ARRAY)
		return prelim * (type->len);

	if(type->flags & TYPE_BUILTIN) {
		return prelim;
	}

	abort();
}

size_t scope_sizeof(struct scope* scope) {
	size_t sz = 0;
	for(size_t i = 0; i < 16; i++) {
		if(scope->usage & (1 << i)) {
			for(struct symbol* t = &scope->symbols[i]; t; t = t->next) {
				sz += expr_sizeof(&t->type);
				t->offset = sz;
			}
		}
	}

	return sz;
}
