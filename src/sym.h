#ifndef SYM_H_
#define SYM_H_

#include "type.h"

#include <stdint.h>
#include <stddef.h>

enum sym_type {
	SYM_TYPE		= (1 << 0),
	SYM_VARIABLE	= (1 << 1),
	SYM_FUNCTION	= (1 << 2),
};

struct symbol {
	struct tok* defined;
	struct symbol* next;
	int global;
	int sym_type;
	size_t iden;
	struct type type;
	size_t offset;
	union {
		struct expr_ast* initializer;
		struct fun_ast* func;
	};
};

#endif
