#ifndef SIZEOF_H_
#define SIZEOF_H_

#include <stddef.h>

#include "scope.h"
#include "type.h"

size_t expr_sizeof(struct type* type);

size_t scope_sizeof(struct scope* scope);

#endif
