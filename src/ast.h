#ifndef AST_H_
#define AST_H_

#include <stddef.h>
#include <stdint.h>

#include "type.h"
#include "scope.h"

enum asttype {
	AST_STMT,
	AST_VARDEF,
	AST_BLOCK,
	AST_IF,
	AST_FUN,
	AST_RETURN,
};

/* expression */
enum expr_type {
	EXPR_NUM,
	EXPR_VAR,
	EXPR_BINOP,
	EXPR_UNOP,
	EXPR_DEREF,
	EXPR_FCALL,
	EXPR_CAST,
	EXPR_STRING,
	EXPR_ARRAY,
	EXPR_SYSCALL,
};

struct syscall_ast {
	struct expr_ast** args;
	size_t n_args;
};

struct fcall_ast {
	struct symbol* func;
	struct expr_ast** args;
};

struct binop_ast {
	struct expr_ast* left;
	struct expr_ast* right;
};

struct expr_ast {
	int exprtype;
	/* associated token */
	struct tok* tok;
	union {
		size_t string;
		struct expr_ast** array;
		struct expr_ast* child;
		struct binop_ast binop;
		struct fcall_ast fcall;
		struct syscall_ast syscall;
		struct symbol* sym;
		uint64_t num;
	};

	int op;

	struct type type;
};

struct block_ast {
	struct ast* code;
	struct scope* scope;
};

struct if_ast {
	struct block_ast block;
	struct expr_ast* cond;
};

struct fun_ast {
	struct symbol* sym;
	struct scope* scope;
	struct ast* code;
	struct type ret_type;
	int is_defined;
	struct symbol** arguements;
	size_t n_args;
};

struct ast {
	struct ast* next;
	struct tok* tok;
	int type;
	union {
		struct expr_ast* expr;
		struct symbol* vardef;
		struct block_ast block;
		struct if_ast if_construct;
		struct fun_ast fun;
	};
};

#endif
