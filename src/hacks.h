#ifndef HACKS_H_
#define HACKS_H_

#include <stdlib.h>
#include <stdio.h>
/* make fatal and error reporting so I don't have to worry about them */
static void* qrealloc(void* p, size_t n) {
	p = realloc(p, n);
	if(!p) {
		perror("realloc");
		exit(1);
	}
	return p;
}

static void* qcalloc(size_t m, size_t n) {
	void* p = calloc(m, n);
	if(!p) {
		perror("realloc");
		exit(1);
	}
	return p;
}

#define realloc(p, n) qrealloc((p), (n))
#define calloc(m, n) qcalloc((m), (n))

#endif
