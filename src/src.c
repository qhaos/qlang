#include "src.h"
#include <stdlib.h>

char** lines = NULL;
size_t n_lines;

/* returns 0 on success, 1 on EOF, and 2 on error */
int addline(FILE* in) {
	int ch = getc(in);
	if(ch == EOF)
		return 1;

	ungetc(ch, in);

	n_lines++;

	lines = realloc(lines, sizeof(char **) * n_lines);

	if(!lines)
		return 2;

	lines[n_lines - 1] = NULL;
	size_t len = 0;
	len = getline(&(lines[n_lines - 1]), &len, in);
	if(len == (size_t) -1)
		return 2;

	return 0;
}


/* returns 0 on success, 1 on error */
int readfile(FILE* in) {
	int e;
	while(!(e = addline(in)))
		;
	if(e == 2)
		return 1;
	else
		return 0;
}

