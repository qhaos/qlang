#ifndef SRC_H_
#define SRC_H_

#include <stdio.h>
#include <stddef.h>

extern char** lines;
extern size_t n_lines;

int readfile(FILE* in);

#endif
