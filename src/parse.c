#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "hacks.h"
#include "iden.h"
#include "parse.h"
#include "print.h"
#include "scope.h"
#include "sizeof.h"
#include "string.h"
#include "tok.h"

#include "opt.h"


/* parser state, kinda gross ngl */
static size_t curtok = 0;
static struct scope* curscope = &global;

static void eofcheck() {
	if(curtok >= n_tokens) {
		print_error(&tokens[n_tokens-1], "unexpected EOF");
	}
}

static struct expr_ast* parse_expr();

static struct expr_ast* parse_paren() {
	curtok++;
	struct expr_ast* ret = parse_expr();
	if(curtok >= n_tokens || tokens[curtok].type != ')')
		print_error(&tokens[curtok-1], "expected ')'");
	curtok++;

	return ret;	
}

static struct expr_ast* parse_syscall() {
	curtok++;
	eofcheck();
	if(tokens[curtok].type != '(') {
		print_error(&tokens[curtok], "expected '(' for syscall arguement list");
	}
	curtok++;

	struct expr_ast* ret = calloc(sizeof(struct expr_ast), 1);

	ret->exprtype = EXPR_SYSCALL;
	ret->type = (struct type) {
		.flags = TYPE_BUILTIN,
		.ptr = 0,
		.key = KEY_U64
	};

	ret->syscall.n_args = 0;
	ret->syscall.args = NULL;
	
	while(tokens[curtok].type != ')') {
		ret->syscall.args = realloc(ret->syscall.args, sizeof(struct expr_ast) * (ret->syscall.n_args+1));
		ret->syscall.args[ret->syscall.n_args] = parse_expr();
		ret->syscall.n_args++;
		if(tokens[curtok].type == ')')
			break;

		if(tokens[curtok].type != ',')
			print_error(&tokens[curtok], "expected ',' to seperate arguements");
		curtok++;
	}

	curtok++;
	return ret;
}

static struct expr_ast* parse_primary() {
	eofcheck();
	switch(tokens[curtok].type) {
		case TOK_STRING: {
			struct expr_ast* ret = calloc(sizeof(struct expr_ast), 1);
			ret->exprtype = EXPR_STRING;
			ret->string = tokens[curtok].string;
			ret->tok = &tokens[curtok];
			ret->type = (struct type){
				.flags = TYPE_BUILTIN | TYPE_ARRAY,
				.ptr = 0,
				.key = KEY_U8,
				.len = stringtbl[tokens[curtok].string].len,
			};
			curtok++;
			return ret;
		}
		break;
		case TOK_IDEN: {
			size_t save = curtok;
			struct expr_ast* ret = calloc(sizeof(struct expr_ast), 1);
			ret->exprtype = EXPR_VAR;
			ret->tok = &tokens[curtok];
			ret->sym = find_sym(curscope, tokens[curtok].iden, SYM_VARIABLE | SYM_FUNCTION);
			if(!ret->sym) {
				print_error(ret->tok, "unrecognized identifier \"%s\"", identbl[ret->tok->iden].data);
				return NULL;
			}
			curtok++;
			if(ret->sym->sym_type == SYM_VARIABLE || tokens[curtok].type != '(') {
				ret->type = ret->sym->type;
				return ret;
			}

			curtok++;

			ret->exprtype = EXPR_FCALL;

			ret->fcall.func = ret->sym;
			ret->type = ret->fcall.func->func->ret_type;

			size_t n_args = ret->fcall.func->func->n_args;

			ret->fcall.args = n_args? calloc(sizeof(struct expr_ast), n_args) : NULL;
			size_t i;
			for(i = 0; i < n_args; i++) {
				if(tokens[curtok].type == ')')
					break;
				ret->fcall.args[i] = parse_expr();
				if(tokens[curtok].type == ')') {
					i++;
					break;
				}
				if(tokens[curtok].type != ',') {
					print_error(&tokens[curtok], "expected ',' to seperate arguements");
				}
				curtok++;
			}

			while(tokens[curtok].type != ')') {
				if(tokens[curtok].type == ')')
					break;
				struct expr_ast* tmp = parse_expr();
				free(tmp);
				if(tokens[curtok].type == ')') {
					i++;
					break;
				}
				if(tokens[curtok].type != ',') {
					print_error(&tokens[curtok], "expected ',' to seperate arguements");
				}
				curtok++;

				i++;
			}
			curtok++;

			if(i != n_args) {
				print_error(&tokens[save], "function %s expected %zu arguements, but was supplied %zu",
					identbl[ret->fcall.func->iden].data, n_args, i);
			}

			return ret;			
		}
		break;
		case TOK_NUM: {
			struct expr_ast* ret = calloc(sizeof(struct expr_ast), 1);
			ret->exprtype = EXPR_NUM;
			ret->num = tokens[curtok].num;
			ret->tok = &tokens[curtok];
			curtok++;
			ret->type = (struct type){
				.flags = TYPE_BUILTIN,
				.ptr = 0,
				.key = KEY_I64,
				.len = 0,
			};
			return ret;
		}
		break;
		/* possibly unary operator */
		case TOK_OP: {
			switch(tokens[curtok].op) {
				case OP_SUB:
				case OP_NOT:
				case OP_LNOT:
				case OP_ADDR:
				break;
				default:
					print_error(&tokens[curtok], "expected '(', literal, unary operator or identifier");
				break;
			}
			struct expr_ast* ret = calloc(sizeof(struct expr_ast), 1);
			ret->exprtype = EXPR_UNOP;
			ret->tok = &tokens[curtok];
			ret->op = ret->tok->op;
			curtok++;
			ret->child = parse_primary();
			ret->type = ret->child->type;
			if(ret->op == OP_ADDR)
				ret->type.ptr++;
			return ret;
		}
		case '(':
			return parse_paren();
		break;
		case '[': {
			struct tok* save = &tokens[curtok];
			struct expr_ast* ret = calloc(sizeof(struct expr_ast), 1);
			ret->exprtype = EXPR_DEREF;
			curtok++;
			ret->child = parse_expr();
			if(tokens[curtok].type != ']') {
				print_error(&tokens[curtok], "expected ']' (to match '[' at %d:%d)", save->row, save->col);
			}

			if(!ret->child->type.ptr) {
				print_error(save, "operand of derefrence must be a pointer type");
				ret->type.ptr++;
			}

			ret->type = ret->child->type;
			ret->type.ptr--;
			curtok++;
			return ret;
		}
		break;
		case TOK_KEYWORD: {
			int key = tokens[curtok].key;
			if(key == KEY_SIZEOF) {
				curtok++;
				struct expr_ast* ret = parse_primary();
				size_t sz = expr_sizeof(&ret->type);
				ret->type = (struct type) { .flags = TYPE_BUILTIN, 0, { .key = KEY_U64 }, .len = 0};
				ret->exprtype = EXPR_NUM;
				ret->tok -= 1;
				ret->num = sz;
				return ret;
			} else if(key == KEY_SYSCALL) {
				return parse_syscall();
			}
		}
		default:
			print_error(&tokens[curtok], "expected '(', `[`, number, binary operator or identifier");
			return NULL;
		break;
	}
}

static struct expr_ast* parse_rhs(struct expr_ast* lhs, int minprec) {
	while(1) {
		if(tokens[curtok].type != TOK_OP
			|| tokens[curtok].op == OP_NOT
			|| tokens[curtok].op == OP_LNOT
			|| tokens[curtok].op == OP_ADDR)
			return lhs;
		int curprec = prec[tokens[curtok].op];
		if(curprec < minprec) {
			return lhs;
		}

		struct tok* optok = &tokens[curtok];
		curtok++;
		struct expr_ast* rhs = parse_primary();
		eofcheck();

		/* check if next token is an operator with higher precendence. */
		if(tokens[curtok].type == TOK_OP
			&& tokens[curtok].op != OP_NOT
			&& tokens[curtok].op != OP_LNOT
			&& tokens[curtok].op != OP_ADDR
			&& curprec < prec[tokens[curtok].op]) {
			rhs = parse_rhs(rhs, curprec+1);
			if(!rhs)
				return NULL;
		}
		struct expr_ast* nextlhs = calloc(sizeof(struct expr_ast), 1);
		nextlhs->exprtype = EXPR_BINOP;
		nextlhs->tok = optok;
		int op = optok->op;
		nextlhs->op = op;
		nextlhs->binop.left = lhs;
		nextlhs->binop.right = rhs;
		nextlhs->type = lhs->type;

		lhs->type.flags &= ~TYPE_CONST;
		if(lhs->type.flags & TYPE_ARRAY) {
			lhs->type.flags &= ~TYPE_ARRAY;
			lhs->type.ptr++;
		}
		rhs->type.flags &= ~TYPE_CONST;
		if(rhs->type.flags & TYPE_ARRAY) {
			rhs->type.flags &= ~TYPE_ARRAY;
			rhs->type.ptr++;
		}

		if(memcmp(&rhs->type, &lhs->type, sizeof(struct type)) == 0)
			goto end;

		if(lhs->type.ptr) {
			if(rhs->type.flags & TYPE_BUILTIN) {
				nextlhs->type = lhs->type;
				goto end;
			}
			print_error(nextlhs->tok, "mismatched types (can only operate numerical types to pointers)");
		} else if(rhs->type.ptr) {
			if(lhs->type.flags & TYPE_BUILTIN) {
				nextlhs->type = rhs->type;
				goto end;
			}
			print_error(nextlhs->tok, "mismatched types (can only operate numerical types to pointers)");
		} else {
			if((lhs->type.flags | rhs->type.flags) & ~TYPE_BUILTIN) {
				print_error(nextlhs->tok, "cannot operate between non builtin types");
				goto end;
			}
			nextlhs->type = (struct type) { .flags = TYPE_BUILTIN, .key = KEY_I64 };
		}

		end:		
		nextlhs->type = (op == OP_EQ || op == OP_LESS || op == OP_MORE || op == OP_LESSEQ || op == OP_MOREEQ)?
			nextlhs->type
			:
			(struct type) { .flags = TYPE_BUILTIN, .key = KEY_I64 }
		;

		lhs = nextlhs;
	}
}

static struct expr_ast* parse_expr() {
	struct expr_ast* lhs = parse_primary();

	if(!lhs)
		return NULL;

	return partial_eval(parse_rhs(lhs, 0));
}

static struct ast* parse_block() {
	struct ast* ret = calloc(sizeof(struct ast), 1);
	ret->type = AST_BLOCK;
	struct tok* save = &tokens[curtok];
	curtok++;
	struct scope* newscope = calloc(sizeof(struct scope), 1);
	newscope->parent = curscope;
	curscope = newscope;
	ret->block.code = parse();
	ret->block.scope = newscope;
	eofcheck();
	if(curtok >= n_tokens || tokens[curtok].type != '}') {
		print_error(save, "unterminated block");
		return NULL;
	}
	curtok++;
	curscope = curscope->parent;
	return ret;
}

static struct ast* parse_if() {
	struct ast* ret = calloc(sizeof(struct ast), 1);
	ret->type = AST_IF;
	curtok++;
	ret->if_construct.cond = parse_expr();
	struct tok* save = &tokens[curtok];
	eofcheck();
	if(tokens[curtok].type != '{') {
		print_error(
			&tokens[curtok], "expected '{' to match if statement (from %d:%d)",
			save->row, save->col
		);
		return NULL;
	}

	curtok++;
	struct scope* newscope = calloc(sizeof(struct scope), 1);
	newscope->parent = curscope;
	curscope = newscope;
	ret->if_construct.block.code = parse();
	ret->if_construct.block.scope = newscope;
	eofcheck();
	if(curtok >= n_tokens || tokens[curtok].type != '}') {
		print_error(save, "unterminated block");
		return NULL;
	}
	curtok++;
	curscope = curscope->parent;
	return ret;
}

static struct type parse_type() {
	struct type ret = {0};

	if(tokens[curtok].type == TOK_KEYWORD && tokens[curtok].key == KEY_CONST) {
		ret.flags |= TYPE_CONST;
		curtok++;
	}

	eofcheck();

	if(tokens[curtok].type == TOK_KEYWORD) {
		switch(tokens[curtok].key) {
			case KEY_U8:
			case KEY_I8:
			case KEY_U16:
			case KEY_I16:
			case KEY_U32:
			case KEY_I32:
			case KEY_U64:
			case KEY_I64:
				ret.flags |= TYPE_BUILTIN;
				ret.key = tokens[curtok].key;
				curtok++;
			break;
			default:
				print_error(&tokens[curtok], "expected typename");
				return ret;
			break;
		}
	} else if(tokens[curtok].type == TOK_IDEN) {
		/* TODO: handle user made types */
		ret.sym = find_sym(curscope, tokens[curtok].iden, SYM_TYPE);
		if(!ret.sym) {
			print_error(&tokens[curtok], "expected typename");
			return ret;
		}
		ret.flags |= TYPE_BUILTIN;
		curtok++;
	} else {
		print_error(&tokens[curtok], "expected typename");
	}

	eofcheck();

	while(tokens[curtok].type == TOK_OP && tokens[curtok].op == OP_STAR) {
		ret.ptr++;
		curtok++;
	}

	if(tokens[curtok].type == '[') {
		ret.flags |= TYPE_ARRAY;
		curtok++;
		if(tokens[curtok].type == ']') {
			curtok++;
			ret.len = 0;
			return ret;
		}

		struct tok* save = &tokens[curtok];

		struct expr_ast* sz = partial_eval(parse_expr());
		if(sz->exprtype != EXPR_NUM) {
			print_error(save, "expected numerical expression in array subscript");
			curtok++;
			return ret;
		}
		ret.len = sz->num;
		free(sz);
		if(!ret.len) {
			print_error(save, "you cannot have a zero length array, perhaps you want a pointer?");
			return ret;
		}

		if(tokens[curtok].type != ']') {
			print_error(save, "expected closing ']'");
			return ret;
		}
		curtok++;
	}

	return ret;
}

static struct expr_ast* parse_array(struct type* type) {
	struct expr_ast* ret = calloc(sizeof(struct expr_ast), 1);

	struct type lhs_type = *type;

	lhs_type.flags &= ~TYPE_ARRAY;

	ret->exprtype = EXPR_ARRAY;

	struct expr_ast** elements = NULL;
	size_t n_elements = 0;

	size_t save = curtok-1;
	curtok++;
	eofcheck();
	while(1) {
		eofcheck();

		size_t save = curtok;

		elements = realloc(elements, sizeof(struct expr_ast*) * (n_elements+1));
		elements[n_elements] = parse_expr();

		if(!is_implicitly_convertable(&lhs_type, &elements[n_elements]->type)) {
			print_error(&tokens[save], "incompatible types for array");
		}

		implicit_cast(&lhs_type, elements[n_elements]);

		n_elements++;
		eofcheck();


		if(tokens[curtok].type != ',' && tokens[curtok].type != '}') {
			print_error(tokens, "expected '}' or ','");
		}  else if(tokens[curtok].type == ',') {
			curtok++;
		}

		if(tokens[curtok].type == '}')
			break;

		eofcheck();
	}
	curtok++;

	if(type->len == 0) {
		type->len = n_elements;
	} else if(type->len != n_elements) {
		print_error(&tokens[save], "array subscript specifies %zu arguements, but %zu supplied", type->len, n_elements);
	}

	if(n_elements == 0) {
		print_error(&tokens[save], "cannot have array of length 0");
	}

	ret->type = *type;

	ret->array = elements;

	return ret;
}

static struct ast* parse_var() {
	int infer = (tokens[curtok].key == KEY_LET);
	struct ast* ret = calloc(sizeof(struct ast), 1);
	ret->type = AST_VARDEF;
	size_t iden = 0;
	struct symbol sym = {0};
	sym.defined = &tokens[curtok];
	if(infer) {
		curtok++;
	} else {
		sym.type = parse_type();
	}
	struct tok* save = &tokens[curtok-2];
	
	sym.sym_type = SYM_VARIABLE;

	if(tokens[curtok].type != TOK_IDEN) {
		print_error(&tokens[curtok], "expected identifier after type");
		return NULL;
	}

	iden = tokens[curtok].iden;

	sym.iden = iden;
	curtok++;
	eofcheck();
	/* check if there is an initializer */
	if(tokens[curtok].type == TOK_OP && tokens[curtok].op == OP_ASSGN) {
		curtok++;
		eofcheck();
		if(tokens[curtok].type == '{') {
			if(infer) {
				print_error(&tokens[curtok], "arrays are too much work to infer for, for now");
			} else {
				sym.initializer = parse_array(&sym.type);
			}
		} else {
			sym.initializer = parse_expr();
		}

		if(infer) {
			sym.type = sym.initializer->type;
		} else {
			int adj_ptrlvll = sym.type.ptr + (!!(sym.type.flags & TYPE_ARRAY));
			int adj_ptrlvlr = sym.initializer->type.ptr + (!!(sym.initializer->type.flags & TYPE_ARRAY));
			if (adj_ptrlvll != adj_ptrlvlr) {
				print_error(save + !(sym.type.flags & TYPE_ARRAY), "array/ptr-ness mismatch");
			}
			if(sym.initializer->type.flags & TYPE_ARRAY) {
				if(sym.type.flags & TYPE_ARRAY) {
					if(sym.type.key != sym.initializer->type.key) {
						print_error(save-1, "array type mismatch");
					} else if(sym.type.len == 0) {
						sym.type.len = sym.initializer->type.len;
					} else if(sym.type.len != sym.initializer->type.len) {
						print_error(
							save,
							"mismatched array lengths (got %zu, expected %zu)",
							sym.type.len,
							sym.initializer->type.len
						);
					}
				}
			}
		}
	} else if(infer) {
		print_error(&tokens[curtok-2], "can't infer type if no initializer is given");
		return NULL;
	}

	ret->vardef = add_sym(curscope, &sym);

	eofcheck();
	if(tokens[curtok].type != ';') {
		print_error(&tokens[curtok], "expected ';'");
		return NULL;
	}
	curtok++;

	return ret;
}

static struct ast* parse_stmt() {
	struct ast* ret = calloc(sizeof(struct ast), 1);
	ret->type = AST_STMT;
	ret->expr = parse_expr();
	eofcheck();
	if(tokens[curtok].type != ';') {
		print_error(&tokens[curtok], "expected ';'");
		return NULL;
	}
	curtok++;
	return ret;
}

struct ast* parse_fn() {
	curtok++;
	eofcheck();
	struct ast* ret = calloc(sizeof(struct ast), 1);
	ret->type = AST_FUN;

	struct symbol sym = { .sym_type = SYM_FUNCTION, .defined = &tokens[curtok], .next = NULL};

	/* setup scope */
	struct scope* newscope = calloc(sizeof(struct scope), 1);
	newscope->parent = &global;

	struct scope* savescope = curscope;
	curscope = newscope;

	ret->fun.ret_type = parse_type();

	if(tokens[curtok].type != TOK_IDEN) {
		print_error(&tokens[curtok], "expected iden");
		goto end;
	}

	struct tok* save = &tokens[curtok];

	sym.iden = tokens[curtok].iden;

	curtok++;
	
	if(tokens[curtok].type != '(') {
		print_error(&tokens[curtok], "expected (");
		goto end;
	}

	curtok++;

	size_t n_args = 0;

	while(tokens[curtok].type != ')') {
		struct symbol sym = {
			.sym_type = SYM_VARIABLE,
			.defined = &tokens[curtok],
			.next = NULL,
			.type = parse_type(),
		};


		if(tokens[curtok].type != TOK_IDEN) {
			print_error(&tokens[curtok], "expected iden");
			goto end;
		}

		sym.iden = tokens[curtok].iden;

		curtok++;

		if(tokens[curtok].type != ')' && tokens[curtok].type != ',') {
			print_error(&tokens[curtok], "expected ',' or ')'");
			goto end;
		}

		n_args++;
		ret->fun.arguements = realloc(ret->fun.arguements, sizeof(struct symbol)*n_args);
		ret->fun.arguements[n_args-1] = add_sym(curscope, &sym);

		if(tokens[curtok].type ==')')
			break;

		curtok++;
	}

	curtok++;

	ret->fun.n_args = n_args;

	ret->fun.is_defined = tokens[curtok].type == '{';

	struct symbol* current = find_sym(&global, sym.iden, SYM_FUNCTION | SYM_VARIABLE);

	if(!current) {
		ret->fun.sym = add_sym(&global, &sym);
		current = ret->fun.sym;
	} else {
		if(current->sym_type == SYM_VARIABLE) {
			print_error(save, "%s already declared as another type of symbol", identbl[save->iden].data);
		} else if(current->func->is_defined && ret->fun.is_defined) {
			print_error(save, "%s is already defined", identbl[save->iden].data);
		} else if(ret->fun.n_args != current->func->n_args) {
			print_error(save, "%s is declared with %zu arguements, not %zu", current->func->n_args, n_args);
		} else {
			for(size_t i = 0; i < n_args; i++) {
				if(!memcmp(&ret->fun.arguements[i]->type, &current->func->arguements[i]->type, sizeof(struct type))) {
					print_error(save, "type mismatch for arguement %zu");
				}
			}
		}
	}

	current->func = &ret->fun;

	if(ret->fun.is_defined) {
		struct tok* save = &tokens[curtok];
		curtok++;
		ret->fun.code = parse();
		eofcheck();
		if(curtok >= n_tokens || tokens[curtok].type != '}') {
			print_error(save, "unterminated block");
			goto end;
		}
		curtok++;		
	} else if(tokens[curtok].type == ';') {
		curtok++;
		ret->fun.code = NULL;
	} else {
		print_error(&tokens[curtok], "expected ';' or '{'");
	}

	sym.next = current->next;
	*current = sym;
end:
	curscope = savescope;

	if(ret->fun.is_defined) {
		// for(size_t i = 0; i < ret->fun.n_args; i++) {
			// add_sym(newscope, ret->fun.arguements[i]);
		// }

		ret->fun.scope = newscope;
		current->func = &ret->fun;
	}
	
	return ret;	
}

struct ast* parse() {
	if(curtok >= n_tokens)
		return NULL;
	struct ast* ret = NULL;

	struct tok* tok = &tokens[curtok];

	switch(tok->type) {
		case TOK_KEYWORD:
			switch(tok->key) {
				case KEY_IF:
					ret = parse_if();
				break;
				case KEY_FN:
					ret = parse_fn();
				break;
				case KEY_RETURN:
					curtok++;
					ret = calloc(sizeof(struct ast), 1);
					ret->type = AST_RETURN;
					ret->expr = parse_expr();
				break;
				case KEY_SYSCALL:
				case KEY_SIZEOF:
					ret = parse_stmt();
				break;
				case KEY_LET:
					ret = parse_var();
				break;
				default:
					/* must be a variable type */
					ret = parse_var();
				break;
			}
		break;
		case '{':
			ret = parse_block();
		break;
		case '}':
			if(!curscope->parent)
				print_error(&tokens[curtok], "'}' without matching '{'");
			return NULL;
		break;
		case ';':
			curtok++;
			return parse();
		break;
		case TOK_IDEN:
			if(find_sym(curscope, tok->iden, SYM_TYPE)) {
				ret = parse_var();
				break;
			}
		case TOK_OP:
		case '(':
			ret = parse_stmt();
		break;
		default:
			print_error(&tokens[curtok], "unexpected token");
			return NULL;
		break;
	}

	if(!ret)
		return NULL;

	ret->tok = tok;
	ret->next = parse();
	return ret;
}
