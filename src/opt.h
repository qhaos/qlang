#ifndef OPT_H_
#define OPT_H_
#include "ast.h"

/* attempt to trim the fat by partially/fully evaluating exprs where possible,
 * discarding unnecessary operations, converting some divisions and multiplcations to shifts,
 * detecting blatant division by zero, etc
 */ 
struct expr_ast* partial_eval(struct expr_ast* root);

#endif
