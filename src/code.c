#include <stdio.h>

#include "hacks.h"
#include "ast.h"
#include "code.h"
#include "iden.h"
#include "opt.h"
#include "print.h"
#include "string.h"
#include "sizeof.h"

static void _abort(int line) {
	printf("%d\n", line);
	exit(1);
}

#define abort() _abort(__LINE__)

enum {
	RAX,
	RBX,
	RCX,
	RDX,
	RBP,
	RSI,
	RDI,
	RSP,
};

int reg_usage[] = {
	0, 0, 0, 0, 5, 0, 0, 99,
	0, 0, 0, 0, 0, 0, 0, 0,
};

const int reg_usage_OG[] = {
	0, 0, 0, 0, 5, 0, 0, 99,
	0, 0, 0, 0, 0, 0, 0, 0,
};

static char* regname[] = {
	"rax",
	"rbx",
	"rcx",
	"rdx",
	"rbp",
	"rsi",
	"rdi",
	"rsp",
	"r8",
	"r9",
	"r10",
	"r11",
	"r12",
	"r13",
	"r14",
	"r15",
};

static char* regname_b[] = {
	"al",
	"bl",
	"cl",
	"dl",
	"sil",
	"dil",
	"bpl",
	"spl",
	"r8b",
	"r9b",
	"r10b",
	"r11b",
	"r12b",
	"r13b",
	"r14b",
	"r15b",
};

static char* regname_w[] = {
	"ax",
	"bx",
	"cx",
	"dx",
	"si",
	"di",
	"bp",
	"sp",
	"r8w",
	"r9w",
	"r10w",
	"r11w",
	"r12w",
	"r13w",
	"r14w",
	"r15w",
};

static char* regname_d[] = {
	"eax",
	"ebx",
	"ecx",
	"edx",
	"esi",
	"edi",
	"ebp",
	"esp",
	"r8d",
	"r9d",
	"r10d",
	"r11d",
	"r12d",
	"r13d",
	"r14d",
	"r15d",
};

static void emit_expr(struct expr_ast* expr, int reg);

static int select_register(int reg) {
	if(reg != -1) {
		if(reg_usage[reg]++)
			printf("\tpush %s\n", regname[reg]);
		return reg;
	}

	reg = 0;
	for(int i = 1; i < 16; i++) {
		if(!reg_usage[reg])
			break;
		if(reg_usage[i] < reg_usage[reg])
			reg = i;
	}

	if(reg_usage[reg]++) {
		printf("\tpush %s\n", regname[reg]);
	}

	return reg;
}

static void deselect_register(int reg) {
	reg_usage[reg]--;
	if(reg_usage[reg]) {
		printf("\tpop %s\n", regname[reg]);
	}
}

static void emit_strings() {
	for(size_t i = 0; i < stringtbl_len; i++) {
		printf("__string_%zu__: db ", i);
		
		for(size_t j = 0; j < stringtbl[i].len; j++) {
			if(j != 0)
				printf(", ");
			printf("%u", stringtbl[i].data[j]);
		}
		puts(", 0");
	}
}


static size_t calc_size_to_reserve(struct ast* root) {
	size_t sz = 0;

	struct fun_ast* fun = &root->fun;

	sz = scope_sizeof(fun->scope);

	for(struct ast* code = fun->code; code; code = code->next) {
		switch(code->type) {
			case AST_STMT:
			case AST_VARDEF:
			break;
			case AST_FUN:
				print_error(code->fun.sym->defined, "dubious legality of defining functions within other functions");
			break;
			case AST_IF:
				sz += scope_sizeof(code->if_construct.block.scope);
			break;
			case AST_BLOCK:
				sz += scope_sizeof(code->block.scope);
			break;
		}
	}
	
	return sz;
}

static int cc_order[] = {
	RDI,
	RSI,
	RDX,
	RCX,
	8,
	9,
};

/* this will double for what we need with syscall, and fcall */
void emit_args(struct expr_ast** args, size_t n_args, int is_syscall) {
	if(n_args - is_syscall > 6) {
		abort();
	}

	if(is_syscall) {
		emit_expr(args[0], RAX);
	}

	for(size_t i = is_syscall; i < n_args; i++) {
		if((~args[i]->type.flags) & TYPE_BUILTIN)
			abort();

		/* grab register */
		select_register(cc_order[i-is_syscall]);
		emit_expr(args[i], cc_order[i-is_syscall]);
	}
}

static void deselect_args(size_t n_args, int is_syscall) {
	for(size_t i = is_syscall; i < n_args; i++) {
		deselect_register(cc_order[i-is_syscall]);
	}	
}

static void emit_fcall(struct expr_ast* expr, int reg) {
	/* return value */
	if(reg != RAX) {
		select_register(RAX);
	}
	/* oh shut up, you've written worse >:( */
	emit_args(expr->fcall.args, expr->fcall.func->func->n_args, 0);
	printf("\tcall %s\n", identbl[expr->fcall.func->iden].data);

	/* release */
	deselect_args(expr->fcall.func->func->n_args, 0);

	if(reg != RAX) {
		printf("\tmov %s, %s\n", regname[reg], regname[RAX]);
		deselect_register(RAX);
	}
}

static void emit_syscall(struct expr_ast* expr, int reg) {
	/* return value */
	if(reg != RAX) {
		select_register(RAX);
	}

	emit_args(expr->syscall.args, expr->syscall.n_args, 1);

	puts("\tsyscall");
	
	/* release */
	deselect_args(expr->syscall.n_args, 1);

	if(reg != RAX) {
		printf("\tmov %s, %s\n", regname[reg], regname[RAX]);
		deselect_register(RAX);
	}
}

static void emit_unop(struct expr_ast* expr, int reg) {
	switch(expr->op) {
		case OP_SUB:
			emit_expr(expr->child, reg);
			printf("\tneg %s\n", regname[reg]);	
		break;
		case OP_NOT:
			emit_expr(expr->child, reg);
			printf("\tnot %s\n", regname[reg]);
		break;
		case OP_LNOT:
			/* looks like crappy code, but I promise that's just because of register dependency */
			if(expr->child->exprtype == EXPR_UNOP && expr->child->op == OP_LNOT) {
				emit_expr(expr->child->child, reg);
				printf(	"\ttest %s, %s\n"
						"\tmov %s, 0\n"
						"\tsetnz %s\n",
				regname[reg], regname[reg], regname[reg], regname_b[reg]);
			} else {
				emit_expr(expr->child, reg);
				printf(	"\ttest %s, %s\n"
						"\tmov %s, 0\n"
						"\tsetz %s\n",
				regname[reg], regname[reg], regname[reg], regname_b[reg]);
			}
		break;
		case OP_ADDR:
			if(expr->child->exprtype != EXPR_VAR) {
				print_error(expr->tok, "object of \'@\' operator must be a variable");
				return;
			}

			if(expr->child->sym->global) {
				printf("\tlea %s, [%s]\n", regname[reg], identbl[expr->child->sym->iden].data);
			} else {
				printf("\tlea %s, [rbp-%zu]\n", regname[reg], expr->child->sym->offset);
			}
		break;
		default:
		abort();
		break;
	}
}

static void emit_binop(struct expr_ast* expr, int reg) {
	/* MASSIVE TODO: IMPLEMENT ASSIGNMENT */
	if(expr->op == OP_ASSGN)
		abort();

	switch(expr->op) {
		case OP_ADD: {
			emit_expr(expr->binop.left, reg);
			int rhs = select_register(-1);
			emit_expr(expr->binop.right, rhs);
			printf("\tadd %s, %s\n", regname[reg], regname[rhs]);
			deselect_register(rhs);
		}
		break;
		case OP_SUB: {
			emit_expr(expr->binop.left, reg);
			int rhs = select_register(-1);
			emit_expr(expr->binop.right, rhs);
			printf("\tsub %s, %s\n", regname[reg], regname[rhs]);
			deselect_register(rhs);
		}
		break;
		case OP_STAR:
		case OP_DIV:
		case OP_MOD:
			abort();
		break;
		case OP_LESSEQ: {
			emit_expr(expr->binop.left, reg);
			int rhs = select_register(-1);
			emit_expr(expr->binop.right, rhs);
			printf(
				"\tcmp %s, %s\n"
				"\tmov %s, 0\n"
				"\tsetbe %s\n",
			regname[reg], regname[rhs], regname[reg], regname_b[reg]);
			deselect_register(rhs);
		}
		break;
		case OP_MOREEQ: {
			emit_expr(expr->binop.left, reg);
			int rhs = select_register(-1);
			emit_expr(expr->binop.right, rhs);
			printf(
				"\tcmp %s, %s\n"
				"\tmov %s, 0\n"
				"\tsetbe %s\n",
			regname[rhs], regname[reg], regname[reg], regname_b[reg]);
		}
		break;
		case OP_LSHFT: {
			emit_expr(expr->binop.left, reg);
			int rhs = select_register(RCX);
			emit_expr(expr->binop.right, rhs);
			printf(
				"\tshl %s, %s\n",
			regname[reg], regname[rhs]);
			deselect_register(rhs);
		}
		break;
		case OP_RSHFT: {
			emit_expr(expr->binop.left, reg);
			int rhs = select_register(RCX);
			emit_expr(expr->binop.right, rhs);
			printf(
				"\tshr %s, %s\n",
			regname[reg], regname[rhs]);
			deselect_register(rhs);
		}
		break;
		case OP_LESS: {
			emit_expr(expr->binop.left, reg);
			int rhs = select_register(-1);
			emit_expr(expr->binop.right, rhs);
			printf(
				"\tcmp %s, %s\n"
				"\tsetb %s\n",
			regname[reg], regname[rhs], regname_b[reg]);
			deselect_register(rhs);
		}
		break;
		case OP_MORE: {
			emit_expr(expr->binop.left, reg);
			int rhs = select_register(-1);
			emit_expr(expr->binop.right, rhs);
			printf(
				"\tcmp %s, %s\n"
				"\tsetg %s\n",
			regname[reg], regname[rhs], regname_b[reg]);
			deselect_register(rhs);
		}
		break;
		case OP_EQ: {
			emit_expr(expr->binop.left, reg);
			int rhs = select_register(-1);
			emit_expr(expr->binop.right, rhs);
			printf(
				"\tcmp %s, %s\n"
				"\tmov %s, 0\n"
				"\tsete %s\n",
			regname[reg], regname[rhs], regname[reg], regname_b[reg]);
			deselect_register(rhs);
		}
		break;
		case OP_NEQ: {
			emit_expr(expr->binop.left, reg);
			int rhs = select_register(-1);
			emit_expr(expr->binop.right, rhs);
			printf(
				"\tcmp %s, %s\n"
				"\tmov %s, 0\n"
				"\tsetne %s\n",
			regname[reg], regname[rhs], regname[reg], regname_b[reg]);
			deselect_register(rhs);
		}
		break;
		case OP_AND: {
			emit_expr(expr->binop.left, reg);
			int rhs = select_register(-1);
			emit_expr(expr->binop.right, rhs);
			printf("\tand %s, %s\n", regname[reg], regname[rhs]);
			deselect_register(rhs);
		}
		break;
		case OP_OR: {
			emit_expr(expr->binop.left, reg);
			int rhs = select_register(-1);
			emit_expr(expr->binop.right, rhs);
			printf("\tor %s, %s\n", regname[reg], regname[rhs]);
			deselect_register(rhs);
		}
		break;
		case OP_XOR:  {
			emit_expr(expr->binop.left, reg);
			int rhs = select_register(-1);
			emit_expr(expr->binop.right, rhs);
			printf("\txor %s, %s\n", regname[reg], regname[rhs]);
			deselect_register(rhs);
		}
		break;
	}

	if(expr->type.ptr)
		return;

	int key = expr->type.key;
	switch(key) {
		case KEY_U64:
		case KEY_I64:
		break;
		default:
			/* odds are signed */
			if(key & 1) {
				printf("\tshl %s, %d\n\tshr %s, %d", regname[reg], 0x40-((8 << (key/2))), regname[reg], 0x40-((8 << (key/2))));
			} else {
				printf("\tand %s, %d\n", regname[reg], (1 << (8 << (key/2))) - 1);
			}
		break;
	}
}

/* expression and register we want the result in */
static void emit_expr(struct expr_ast* expr, int reg) {
	switch(expr->exprtype) {
		case EXPR_UNOP:
			emit_unop(expr, reg);
		break;
		case EXPR_NUM:
			if(reg == -2)
				return;

			if(expr->num == 0) {
				printf("\txor %s, %s\n", regname[reg], regname[reg]);
				return;
			}

			switch(expr->type.key) {
				case KEY_U8:
					printf("\txor %s, %s\n", regname[reg], regname[reg]);
					printf("\tmov %s, %zu\n", regname_b[reg], expr->num & 0xFF);
				break;
				case KEY_U16:
					printf("\txor %s, %s\n", regname[reg], regname[reg]);
					printf("\tmov %s, %zu\n", regname_w[reg], expr->num & 0xFFFF);
				break;
				case KEY_U32:
					printf("\txor %s, %s\n", regname[reg], regname[reg]);
					printf("\tmov %s, %zu\n", regname_d[reg], expr->num & 0xFFFFFFFF);
				break;
				case KEY_I8:
				case KEY_I16:
				case KEY_I32:
				case KEY_I64:
				case KEY_U64:
					printf("\tmov %s, %zu\n", regname[reg], expr->num);
				break;
				default:
				abort();
			}
		break;
		case EXPR_DEREF:
			if(reg == -2)
				return;
			emit_expr(expr->child, reg);
			printf("\tmov %s, [%s]\n", regname[reg], regname[reg]);
		break;
		case EXPR_SYSCALL:
			emit_syscall(expr, reg == -2? RAX : reg);
		break;
		case EXPR_FCALL:
			if(reg == -2) {
				emit_fcall(expr, select_register(RAX));
				deselect_register(RAX);
			} else {
				emit_fcall(expr, reg);
			}
		break;
		case EXPR_STRING:
			if(reg == -2)
				return;
			printf("\tmov %s, __string_%zu__\n", regname[reg], expr->string);
		break;
		case EXPR_VAR:
			if(expr->sym->type.flags & TYPE_ARRAY) {
				printf("\tlea ");
			} else {
				printf("\tmov ");
			}
			printf("%s, [", regname[reg]);
			if(expr->sym->global)
				printf("%s", identbl[expr->sym->iden].data);
			else
				printf("rbp - %zu", expr->sym->offset);
			puts("]");
		break;
		case EXPR_BINOP:
			emit_binop(expr, reg);
		break;
		default:
			print_error(expr->tok, "unimplemented");
		break;
	}
}


static void emit_block(struct ast* code) {
	if(!code)
		return;

	switch(code->type) {
		case AST_BLOCK:
			emit_block(code->block.code);
		break;
		case AST_STMT:
			emit_expr(code->expr, -2);
		break;
		case AST_RETURN:
			emit_expr(code->expr, RAX);
			puts("\tleave\n\tret");
		break;
		case AST_VARDEF: {
			if(!code->vardef->initializer)
				break;
			/* TODO: finish for arrays */
			int reg = select_register(-1);
			emit_expr(code->vardef->initializer, reg);
			printf("\tmov [rbp-%zu], %s\n", code->vardef->offset, regname[reg]);
		}
		break;
		case AST_IF: {
			/* for the name of the label */
			int reg = select_register(-1);
			emit_expr(code->if_construct.cond, reg);
			deselect_register(reg);
			printf("\ttest %s, %s\n\tjz .L%d_%d\n", regname[reg], regname[reg], code->tok->row, code->tok->col);
			emit_block(code->if_construct.block.code);
			printf(".L%d_%d:\n", code->tok->row, code->tok->col);
		}
		break;
		default:
			print_error(code->tok, "unimplemented");
			abort();
		break;
	}

	emit_block(code->next);
}

static void emit_function(struct ast* func) {
	size_t reserve = calc_size_to_reserve(func);

	printf("%s:\n", identbl[func->fun.sym->iden].data);

	puts("\tpush rbp\n\tmov rbp, rsp");


	/* round it off to the nearest multiple of 16 */
	if(reserve & 0xF) {
		reserve >>= 4;
		reserve += 1;
		reserve <<= 4;
	}

	if(reserve) {
		printf("\tsub rsp, %zu\n", reserve);
	}

	struct symbol** arguements = func->fun.arguements;
	size_t n_args = func->fun.n_args;

	if(n_args > 6) {
		abort();
	}

	for(size_t i = 0; i < n_args; i++) {
		if(arguements[i]->type.flags & TYPE_ARRAY) {
			abort();
		} else {
			size_t sz = expr_sizeof(&arguements[i]->type);
			printf("\tmov [rbp - %zu], %s\n", arguements[i]->offset,
				(sz == 1)?
					regname_b[cc_order[i]]
					: (sz == 2)?
						regname_w[cc_order[i]]
						: (sz == 4)?
							regname_d[cc_order[i]]
							: regname[cc_order[i]]
			);
		}
	}

	emit_block(func->fun.code);

	puts("\tleave\n\tret");
}

static void emit_code(struct ast* code) {
	if(!code) {
		return;
	}

	if(code->fun.is_defined)
		emit_function(code);

	emit_code(code->next);
}

/* ensure top level declarations are only variable declarations and functions */
static void check_toplevel(struct ast* code) {
	switch(code->type) {
		case AST_IF:
			print_error(code->tok, "if statement not allowed outside function");
		break;
		case AST_RETURN:
			print_error(code->tok, "return statement not allowed outside function");
		break;
		case AST_BLOCK:
			print_error(code->tok, "block not allowed outside function");
		break;
		case AST_STMT:
			print_error(code->tok, "statement not allowed outside function");
		break;
	}

	check_toplevel(code->next);
}

static const char* size_chart[] = {
	"db",
	"db",
	"dw",
	"dw",
	"dd",
	"dd",
	"dq",
	"dq"
};

static void emit_data(struct ast* code) {
	if(!code) {
		return;
	}

	if(code->type != AST_VARDEF) {
		goto end;
	}

	
	printf("%s: ", identbl[code->vardef->iden].data);

	if(!code->vardef->initializer) {
		putchar('\n');
		goto end;
	}

	struct type* type = &code->vardef->type;

	size_t sz = type->key;
	
	if(type->ptr)
		sz = 6;

	printf("%s", size_chart[sz]);

	if(type->flags & TYPE_ARRAY) {
		if(code->vardef->initializer->exprtype == EXPR_STRING) {
			printf(" __string_%zu__", code->vardef->initializer->string);
		} else {
			for(size_t i = 0; i < type->len; i++) {
				if(i != 0)
					putchar(',');

				struct expr_ast* expr = code->vardef->initializer->array[i];

				if(expr->exprtype == EXPR_STRING) {
					printf(" __string_%zu__", expr->string);
				} else if(expr->exprtype == EXPR_NUM) {
					printf(" %zu", expr->num);
				} else {
					print_error(expr->tok, "initializer must be constant");
				}
			}
		}
	} else {
		struct expr_ast* expr = code->vardef->initializer;

		if(expr->exprtype == EXPR_STRING) {
			printf(" __string_%zu__", expr->string);
		} else if(expr->exprtype == EXPR_NUM) {
			printf(" %zu", expr->num);
		} else {
			print_error(expr->tok, "initializer must be constant");
		}
	}

	putchar('\n');

end:
	emit_data(code->next);
}


void gen(struct ast* code) {
	puts("format ELF64 executable");

	puts("segment readable");
	emit_strings();

	puts("segment readable writeable");
	emit_data(code);

	puts("segment readable executable");
	puts("entry _start");
	emit_code(code);
}
