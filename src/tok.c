#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "hacks.h"
#include "iden.h"
#include "string.h"
#include "src.h"
#include "tok.h"

static int row = 0;
static int col = 0;
struct tok* tokens = NULL;
size_t n_tokens;
static struct tok *tail = NULL;

static int isop() {
	return strchr("+-*/%><=~!|^&@", lines[row][col]) != NULL;
}

static int addop() {
	for(size_t i = 0; i < sizeof(ops)/sizeof(char*); i++) {
		if(!strncmp(ops[i], &lines[row][col], strlen(ops[i]))) {
			tail->type = TOK_OP;
			tail->op = i;
			col += strlen(ops[i]);
			return 0;
		}
	}
	abort();
}

static void addhex() {
	while(isxdigit(lines[row][col])) {
		tail->num <<= 4;
		if(isdigit(lines[row][col]))
			tail->num |= lines[row][col] - '0';
		else
			tail->num |= 10 + toupper(lines[row][col]) - 'A';
		col++;
	}
}

/* returns 0 on success, 1 on error */
static int addnum() {
	tail->type = TOK_NUM;
	tail->num = 0;

	/* check for hex */
	if(lines[row][col] == '0' && lines[row][col + 1] == 'x') {
		col += 2;
		addhex();
	} else {
		while (isdigit(lines[row][col])) {
			tail->num *= 10;
			tail->num += lines[row][col] - '0';
			col++;
		}
	}

	if(isalpha(lines[row][col])) {
		while(isalnum(lines[row][col]))
			col++;

		tail->type = TOK_ERR;
		tail->err = ERR_INVNUM;
		return 1;
	}

	return 0;
}


static int escape(char* ret) {
	switch(lines[row][col]) {
		case '\'':
			*ret = '\'';
		break;
		case '\"':
			*ret = '\"';
		break;
		case '\\':
			*ret = '\\';
		break;
		case 'a':
			*ret = '\a';
		break;
		case 'b':
			*ret = '\b';
		break;
		case 'f':
			*ret = '\f';
		break;
		case 'n':
			*ret = '\n';
		break;
		case 'r':
			*ret = '\r';
		break;
		case 't':
			*ret = '\t';
		break;
		case 'v':
			*ret = '\v';
		break;
		case 'x': {
			uint8_t x = 0;
			const char* hexstr = "0123456789ABCDEF";
			x |= index(hexstr, toupper(lines[row][col])) - hexstr;
			if(!isxdigit(lines[row][col++]))
				return 1;
			x <<= 4;
			x |= index(hexstr, toupper(lines[row][col])) - hexstr;
			if(!isxdigit(lines[row][col]))
				return 1;
			*ret = x;
		}
		break;
		default:
			return 1;
		break;
	}
	return 0;
}

static char* append_str(char* str, size_t len, char ch) {
	str = realloc(str, len+1);
	if(ch == '\\') {
		col++;
		if(escape(&ch))
			return NULL;
	}
	str[len] = ch;
	return str;
}

static long int addstring() {
	char* str = NULL;
	long int len = 0;

	while(lines[row][col] != '"') {
		if(lines[row][col] == '\n') {
			return -1;
		}
		int save = col;
		str = append_str(str, len, lines[row][col]);
		if(!str) {
			tail->col = save;
			return -2;
		}
		len++;
		col++;
	}

	str = append_str(str, len, '\0');

	stringtbl = realloc(stringtbl, sizeof(struct string) * (stringtbl_len+1));
	stringtbl[stringtbl_len].data = str;
	stringtbl[stringtbl_len].len = len;
	stringtbl_len++;

	return stringtbl_len-1;
}

/* returns 0 on success, 1 on error */
static int addiden() {
	/* fnv1 */
	uint64_t hash = 0xcbf29ce484222325;
	const char* save = &lines[row][col];
	size_t len = 0;
	while(isalnum(lines[row][col]) || lines[row][col] == '_') {
		hash = (hash ^ lines[row][col]) * 0x100000001b3;
		col++;
		len++;
	}

	for(size_t i = 0; i < sizeof(keywords)/sizeof(char*); i++) {
		if(strlen(keywords[i]) == len && strncmp(keywords[i], save, len) == 0) {
			tail->type = TOK_KEYWORD;
			tail->key = i;
			return 0;
		}
	}

	tail->type = TOK_IDEN;
	tail->iden = register_iden(save, len, hash);

	return 0;
}

/* number of erros */
int lex() {
	if((size_t)row >= n_lines) {
		/* it's all over */
		row = 0;
		col = 0;
		tail = NULL;
		return 0;
	}
	const char *curr = lines[row];

	while (isspace(curr[col]))
		col++;

	if(!curr[col] || (curr[col] == '/' && curr[col+1] == '/')) {
		col = 0;
		row++;
		return lex();
	}

	n_tokens++;
	tokens = realloc(tokens, n_tokens * sizeof(struct tok));

	tail = &tokens[n_tokens - 1];

	tail->row = row;
	tail->col = col;

	int e = 0;

	int str_flag = 0;

	if(isdigit(curr[col])) {
		e = addnum();
	} else if(isalpha(curr[col]) || curr[col] == '_') {
		e = addiden();
	} else if(isop(curr[col])) {
		e = addop();
	} else {
		switch(curr[col]) {
			case '(':
			case ')':
			case '[':
			case ']':
			case ';':
			case '{':
			case '}':
			case ',':
				tail->type = lines[row][col];
			break;
			case '\'':
				col++;
				tail->num = 0;
				while(lines[row][col] != '\'') {
					char i = lines[row][col];
					col++;
					if(i == '\\') {
						if(escape(&i))
							goto invesc;
					}
					tail->num <<= 8;
					tail->num |= (unsigned char) i;
				}

				tail->type = TOK_NUM;
				break;

				invesc:
				tail->type = TOK_ERR;
				tail->err = ERR_INVESCAPE;
			break;
			case '\"':
				str_flag = 1;
				col++;
				long int str = addstring();
				if(str == -1) {
					tail->type = TOK_ERR;
					tail->err = ERR_UNTERMINATEDSTRING;
					e = 1;
				} else if(str == -2){
					tail->type = TOK_ERR;
					tail->err = ERR_INVESCAPE;
					e = 1;
				} else {
					tail->type = TOK_STRING;
					tail->string = str;
				}
			break;
			default:
				tail->type = TOK_ERR;
				tail->err = ERR_UNRECOGNIZED;
				e = 1;
			break;
		}
		col++;
	}

	tail->len = col - tail->col - str_flag;

	return lex() + !!e;
}
