#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "opt.h"
#include "print.h"
#include "sym.h"
#include "type.h"

void eval_binops(struct expr_ast* root) {
	if(root->binop.left->exprtype == EXPR_NUM && root->binop.right->exprtype == EXPR_NUM) {
		root->exprtype = EXPR_NUM;
		uint64_t lhs = root->binop.left->num;
		uint64_t rhs = root->binop.right->num;
		free(root->binop.left);
		free(root->binop.right);
		root->num = lhs;
		switch(root->op) {
			case OP_ADD:
				root->num += rhs;
			break;
			case OP_SUB:
				root->num -= rhs;
			break;
			case OP_STAR:
				root->num *= rhs;
			break;
			case OP_DIV:
				if(rhs == 0)
					print_error(root->tok, "attempt to divide by zero ☹");
				else
					root->num /= rhs;
			break;
			case OP_MOD:
				if(rhs == 0)
					print_error(root->tok, "attempt to divide by zero ☹");
				else
					root->num %= rhs;
			break;
			case OP_LESSEQ:
				root->num = root->num <= rhs;
			break;
			case OP_MOREEQ:
				root->num = root->num >= rhs;
			break;
			case OP_LSHFT:
				root->num <<= rhs;
			break;
			case OP_RSHFT:
				root->num >>= rhs;
			break;
			case OP_LESS:
				root->num = root->num < rhs;
			break;
			case OP_MORE:
				root->num ^= root->num > rhs;
			break;
			case OP_EQ:
				root->num ^= root->num == rhs;
			break;
			case OP_NEQ:
				root->num ^= root->num != rhs;
			break;
			case OP_ASSGN:
				print_error(root->tok, "attempt to assign to a numerical expression");
			break;
			case OP_AND:
				root->num &= rhs;
			break;
			case OP_OR:
				root->num |= rhs;
			break;
			case OP_XOR:
				root->num ^= rhs;
			break;
			default:
				fprintf(stderr, "compiler broke\n");
				exit(1);
			break;
		}
	} else if(root->binop.left->exprtype == EXPR_NUM) {
		switch(root->op) {
			case OP_STAR:
				if(root->binop.left->num == 1) {
					void* savel = root->binop.left;
					void* saver = root->binop.right;
					*root = *root->binop.right;
					free(savel);
					free(saver);
					return;
				}
			case OP_RSHFT:
			case OP_LSHFT:
				if(root->binop.left->num == 0) {
					free(root->binop.left);
					free(root->binop.right);
					root->exprtype = EXPR_NUM;
					root->num = 0;
					return;
				}
			break;
			case OP_ADD:
			case OP_SUB:
				if(root->binop.left->num == 0) {
					void* savel = root->binop.left;
					void* saver = root->binop.right;
					*root = *root->binop.right;
					free(savel);
					free(saver);
				}
			break;
		}
	} else if(root->binop.right->exprtype == EXPR_NUM) {
		switch(root->op) {
			case OP_STAR:
			case OP_DIV:
				if(root->binop.right->num == 0) {
					if(root->op == OP_DIV) {
						print_error(root->tok, "attempt to divide by zero ☹");
						return;
					}
					free(root->binop.left);
					free(root->binop.right);
					root->exprtype = EXPR_NUM;
					root->num = 0;
					return;
				} else if(root->binop.right->num == 1) {
					void* savel = root->binop.left;
					void* saver = root->binop.right;
					*root = *root->binop.left;
					free(savel);
					free(saver);					
				} else if(__builtin_popcountl(root->binop.right->num) == 1) {
					/* optimize to a shift */
					root->op = root->op == OP_DIV? OP_RSHFT : OP_LSHFT;
					root->binop.right->num = 63 - __builtin_clzl(root->binop.right->num);
				}
			break;
			case OP_MOD:
				if(root->binop.right->num == 0) {
					print_error(root->tok, "attempt to divide by zero ☹");
					return;
				}
			break;
			case OP_RSHFT:
			case OP_LSHFT:
				if(root->binop.right->num > 63) {
					free(root->binop.left);
					free(root->binop.right);
					root->exprtype = EXPR_NUM;
					root->num = 0;
					return;
				}
			case OP_ADD:
			case OP_SUB:
				if(root->binop.right->num == 0) {
					void* savel = root->binop.left;
					void* saver = root->binop.right;
					*root = *root->binop.left;
					free(savel);
					free(saver);
				}
			break;
		}
	}

}

/* attempt to trim the fat by partially/fully evaluating exprs where possible */ 
struct expr_ast* partial_eval(struct expr_ast* root) {
	switch(root->exprtype) {
		case EXPR_VAR:
		case EXPR_NUM:
		break;
		case EXPR_DEREF:
			root->child = partial_eval(root->child);
		break;
		case EXPR_UNOP: {
			root->child = partial_eval(root->child);
			if(root->child->exprtype == EXPR_NUM) {
				root->exprtype = EXPR_NUM;
				uint64_t save = root->child->num;
				free(root->child);
				root->num = save;
				switch(root->op) {
					case OP_NOT:
						root->num = ~root->num;
					break;
					case OP_SUB:
						root->num = -root->num;
					break;
					case OP_LNOT:
						root->num = !root->num;
					break;
					case OP_ADDR:
						print_error(root->tok, "cannot apply unary operator to numerical expression");
					break;
					default:
						assert("compiler broke" && 0);
					break;
				}
			}
			return root;
		}
		break;
		case EXPR_BINOP: {
			root->binop.left = partial_eval(root->binop.left);
			root->binop.right = partial_eval(root->binop.right);
			eval_binops(root);
			return root;
		}
		break;
	}
	return root;
}

